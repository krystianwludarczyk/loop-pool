import { ThrottlerException, ThrottlerGuard } from '@nestjs/throttler';

export class DefaultThrottlerGuard extends ThrottlerGuard {
  protected async throwThrottlingException(): Promise<void> {
    throw new ThrottlerException('Wysyłasz zbyt wiele żądań.');
  }
}
