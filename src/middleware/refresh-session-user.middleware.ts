import { Injectable, NestMiddleware, Req, Res } from '@nestjs/common';
import { NextFunction } from 'express';
import { UsersService } from 'src/api/users/users.service';
import { LogsService } from 'src/services/logger/logs.service';

@Injectable()
export class RefreshSessionUserMiddleware implements NestMiddleware {
  constructor(
    private readonly userService: UsersService,
    private readonly logsService: LogsService,
  ) {}

  async use(@Req() req, @Res() res, next: NextFunction) {
    try {
      let user = '?';
      if (req.session && req.session.user && req.session.user.username) {
        user = req.session.user.username;
      }
      this.logsService.createConsoleLog(`[${user}] [${req.method}] : ${req.originalUrl}`);

      if (req.session && req.session.user && req.session.user.id) {
        const userId = req.session.user.id;
        const user = await this.userService.getById(userId);

        if (user) {
          user.lastOnline = new Date();
          req.session.user = await this.userService.update(user);
        } else {
          req.session.destroy();
        }
      }
    } catch (error) {
      console.error(error);
    }
    next();
  }
}
