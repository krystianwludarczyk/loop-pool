import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateCommentDto {
  @IsNotEmpty({ message: 'Komentarz nie może być pusty.' })
  @IsString()
  @MaxLength(300, { message: 'Komentarz nie może być dłuższy niż 300 znaków.' })
  content: string;
}
