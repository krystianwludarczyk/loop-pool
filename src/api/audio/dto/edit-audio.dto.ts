import { IsBoolean, IsEnum, IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { Key } from '../enum/key.enum';
import { KeyType } from '../enum/keyType.enum';
import { Category } from '../enum/category.enum';

export class EditAudioDto {
  @IsString()
  @MaxLength(300, {
    message: 'URL zdjęcia nie może być dłuższy niż 300 znaków.',
  })
  imageUrl: string;

  @IsNotEmpty({ message: 'Tytuł nie może być pusty.' })
  @IsString()
  @MaxLength(100, { message: 'Tytuł nie może być dłuższy niż 100 znaków.' })
  title: string;

  @IsString()
  @MaxLength(500, { message: 'Opis nie może być dłuższy niż 500 znaków.' })
  description: string;

  @IsEnum(Category, { message: 'Niepoprawna kategoria.' })
  category: Category;

  @IsNotEmpty({ message: 'Tempo nie może być puste.' })
  bpm: number;

  @IsEnum(Key, { message: 'Niepoprawny klucz.' })
  key: Key;

  @IsEnum(KeyType, { message: 'Niepoprawny klucz.' })
  keyType: KeyType;

  @IsBoolean()
  isPrivate: boolean;
}
