import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { StorageModule } from 'src/services/storage/storage.module';
import { AudioController } from './audio.controller';
import { AudioService } from './audio.service';
import { Audio } from './entity/audio.entity';
import { Comment } from './entity/comment.entity';
import { LogsModule } from 'src/services/logger/logs.module';
import { NotificationsModule } from 'src/services/notifications/notifications.module';
import { PermissionsModule } from 'src/services/permissions/permissions.module';
import { CommentsService } from './comments.service';

@Module({
  imports: [
    UsersModule,
    TypeOrmModule.forFeature([Audio, Comment]),
    StorageModule,
    LogsModule,
    NotificationsModule,
    PermissionsModule,
  ],
  controllers: [AudioController],
  providers: [AudioService, CommentsService],
  exports: [AudioService, CommentsService],
})
export class AudioModule {}
