export enum Category {
  Beats = 'Bity',
  LoopsAndSamples = 'Loopy i Sample',
  Vocals = 'Wokale',
  Others = 'Inne',
}
