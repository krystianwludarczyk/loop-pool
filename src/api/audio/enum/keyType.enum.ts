export enum KeyType {
  Unknown = 'Nieznany',
  Minor = 'Minor',
  Major = 'Major',
}
