import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from '../../users/entity/user.entity';
import { Audio } from './audio.entity';

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.comments)
  author: User;

  @ManyToOne(() => Audio, (audio) => audio.comments, { onDelete: 'CASCADE' })
  audioParent: Audio;

  @Column()
  content: string;

  @Column()
  createdAt: Date;
}
