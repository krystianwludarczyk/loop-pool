import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { User } from '../../users/entity/user.entity';
import { Category } from '../enum/category.enum';
import { Key } from '../enum/key.enum';
import { KeyType } from '../enum/keyType.enum';
import { Comment } from 'src/api/audio/entity/comment.entity';

@Entity()
export class Audio {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  imageUrl: string;

  @Column()
  createdAt: Date;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  fileKey: string;

  @ManyToOne(() => User, (user) => user.audio)
  author: User;

  @Column()
  category: Category;

  @Column()
  bpm: number;

  @Column()
  key: Key;

  @Column()
  keyType: KeyType;

  @ManyToMany(() => User, (user) => user.allowedAudio)
  authorizedUsers: User[];

  @Column({ default: false })
  isPrivate: boolean;

  @ManyToMany(() => User, (user) => user.likedAudio)
  likedBy: User[];

  @OneToMany(() => Comment, (comment) => comment.audioParent)
  comments: Comment[];
}
