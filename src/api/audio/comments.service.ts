import { Injectable } from '@nestjs/common';
import { Comment } from './entity/comment.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CommentNotFoundException } from '../../exceptions/comments/comment-not-found.exception';
import { SortBy } from 'src/enum/sortBy.enum';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Comment)
    private commentsRepository: Repository<Comment>,
  ) {}

  async create(commentData: Omit<Comment, 'id'>): Promise<Comment> {
    return await this.commentsRepository.save(commentData);
  }

  async getByIdOrFail(commentId: number, loadRelations = false): Promise<Comment> {
    const comment = await this.commentsRepository.findOne({
      where: { id: commentId },
      relations: loadRelations ? ['author', 'audioParent'] : null,
    });

    if (!comment) {
      throw new CommentNotFoundException();
    }

    return comment;
  }

  async getByQuery(
    query: Record<string, any>,
    page = 1,
    pageSize = 20,
    loadRelations = false,
  ): Promise<{ commentsData: Comment[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Comment> =
      this.commentsRepository.createQueryBuilder('comment');

    if (loadRelations) {
      queryBuilder = queryBuilder
        .leftJoinAndSelect('comment.author', 'author')
        .leftJoinAndSelect('comment.audioParent', 'audioParent');
    }

    Object.entries(query).forEach(([key, value]) => {
      switch (key) {
        case 'keywords':
          queryBuilder = queryBuilder.andWhere('comment.content LIKE :keywords', {
            keywords: `%${value}%`,
          });
          break;
      }
    });

    switch (query.sortBy) {
      case SortBy.Newest:
        queryBuilder = queryBuilder.orderBy('comment.createdAt', 'DESC');
        break;
      case SortBy.Oldest:
        queryBuilder = queryBuilder.orderBy('comment.createdAt', 'ASC');
        break;
      default:
        queryBuilder = queryBuilder.orderBy('comment.createdAt', 'DESC');
        break;
    }

    const totalResults: number = await queryBuilder.getCount();
    const totalPages: number = Math.ceil(totalResults / pageSize);
    queryBuilder.skip((page - 1) * pageSize).take(pageSize);
    const commentsData: Comment[] = await queryBuilder.getMany();

    return { commentsData, totalPages };
  }

  async delete(commentId: number): Promise<void> {
    const commentData = await this.getByIdOrFail(commentId);
    await this.commentsRepository.remove(commentData);
  }

  /* HELPERS */
  async getCommentsByAudio(
    audioId: number,
    page = 1,
    pageSize = 20,
    loadRelations = false,
  ): Promise<{ commentsData: Comment[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Comment> =
      this.commentsRepository.createQueryBuilder('comment');

    if (loadRelations) {
      queryBuilder = queryBuilder
        .leftJoinAndSelect('comment.author', 'author')
        .leftJoinAndSelect('comment.audioParent', 'audioParent');
    }

    queryBuilder = queryBuilder
      .where('comment.audioParentId = :audioId', { audioId })
      .orderBy('comment.createdAt', 'ASC')
      .skip((page - 1) * pageSize)
      .take(pageSize);

    const [comments, count] = await queryBuilder.getManyAndCount();

    return { commentsData: comments, totalPages: Math.ceil(count / pageSize) };
  }

  async getCommentPageNumber(audioId: number, commentId: number, pageSize = 20): Promise<number> {
    const comment = await this.commentsRepository.exists({
      where: { id: commentId },
    });

    if (!comment) {
      throw new CommentNotFoundException();
    }

    const comments: Comment[] = await this.commentsRepository
      .createQueryBuilder('comment')
      .leftJoinAndSelect('comment.audioParent', 'audioParent')
      .where('audioParent.id = :audioId', { audioId })
      .orderBy('comment.createdAt', 'ASC')
      .getMany();

    const commentPosition: number = comments.findIndex((comment) => comment.id === commentId);

    return Math.ceil((commentPosition + 1) / pageSize);
  }
}
