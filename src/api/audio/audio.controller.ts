import {
  Controller,
  UseGuards,
  Req,
  Body,
  Post,
  UseInterceptors,
  Param,
  ParseIntPipe,
  Get,
  Res,
  UploadedFile,
  Patch,
  Delete,
  BadRequestException,
} from '@nestjs/common';
import { AuthenticatedGuard } from 'src/api/auth/guard/authenticated.guard';
import { CreateAudioDto } from './dto/create-audio.dto';
import AudioFileInterceptor from './interceptor/audio.interceptor';
import { AudioService } from './audio.service';
import { StorageService } from 'src/services/storage/storage.service';
import { ConfigService } from '@nestjs/config';
import { EditAudioDto } from './dto/edit-audio.dto';
import { formatTextareaToString } from 'src/helpers/format.helper';
import { LogsService } from 'src/services/logger/logs.service';
import { UsersService } from '../users/users.service';
import { NotificationsService } from 'src/services/notifications/notifications.service';
import { validateImageUrl } from 'src/helpers/validator.helper';
import { PermissionsService } from 'src/services/permissions/permissions.service';
import { ActionType } from 'src/services/permissions/action.enum';
import { Audio } from './entity/audio.entity';
import { CreateCommentDto } from './dto/create-comment.dto';
import { Comment } from './entity/comment.entity';
import { CommentsService } from './comments.service';
import { FileBufferEmptyException } from '../../exceptions/files/file-buffer-empty.exception';
import { AudioNotPrivateException } from 'src/exceptions/audio/audio-not-private.exception';

@Controller()
export class AudioController {
  constructor(
    private readonly audioService: AudioService,
    private readonly commentsService: CommentsService,
    private readonly storageService: StorageService,
    private readonly configService: ConfigService,
    private readonly logsService: LogsService,
    private readonly usersService: UsersService,
    private readonly notificationsService: NotificationsService,
    private readonly permissionsService: PermissionsService,
  ) {}

  @UseGuards(AuthenticatedGuard)
  @UseInterceptors(AudioFileInterceptor)
  @Post('/audio')
  async createAudio(
    @UploadedFile() audioFile: Express.Multer.File,
    @Body() body: CreateAudioDto,
    @Req() req,
  ) {
    if (!audioFile || !audioFile.buffer || audioFile.buffer.length === 0) {
      throw new FileBufferEmptyException();
    }

    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    await this.permissionsService.validateAudioAction(ActionType.Create, {} as Audio, asUserData);

    const uploadedFileKey = await this.storageService.uploadFile(
      audioFile,
      this.configService.getOrThrow('S3_AUDIO_BUCKET_NAME'),
    );

    const isUserPremium = this.usersService.isUserPremium(asUserData);
    const createdAudioData = await this.audioService.create({
      imageUrl: validateImageUrl(body.imageUrl, isUserPremium),
      createdAt: new Date(),
      title: body.title,
      description: formatTextareaToString(body.description),
      fileKey: uploadedFileKey,
      author: asUserData,
      category: body.category,
      bpm: body.bpm,
      key: body.key,
      keyType: body.keyType,
      likedBy: [],
      comments: [],
      isPrivate: false,
      authorizedUsers: [],
    });

    const userFollowers = await this.usersService.getUserFollowers(asUserData.id);
    for (const follower of userFollowers) {
      await this.notificationsService.createNotification(
        `/audio/${createdAudioData.id}`,
        `<b>${createdAudioData.author.username}</b> opublikował plik <b>${createdAudioData.title}</b>`,
        follower.id,
      );
    }

    await this.logsService.createLog(
      `USER[${asUserData.id}] CREATED AUDIO[${createdAudioData.id}]`,
      ``,
      `${JSON.stringify(createdAudioData)}`,
    );

    return createdAudioData;
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/audio/:id/download')
  async downloadAudio(@Param('id', ParseIntPipe) audioId: number, @Res() res, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);

    const audioData = await this.audioService.getByIdOrFail(audioId);
    const audioAuthor = await this.audioService.getAudioAuthor(audioId);
    const authorizedUsers = await this.audioService.getAudioAuthorizedUsers(audioId);

    await this.permissionsService.validateAudioAction(
      ActionType.Read,
      { ...audioData, author: audioAuthor, authorizedUsers },
      asUserData,
    );

    const bucketName = this.configService.getOrThrow('S3_AUDIO_BUCKET_NAME');
    const audioFileStream = await this.storageService.createReadStream(
      audioData.fileKey,
      bucketName,
    );

    res.setHeader('Content-Type', audioFileStream.mimetype);
    res.setHeader('Content-Length', audioFileStream.length);
    audioFileStream.pipe(res);

    await this.logsService.createLog(
      `USER[${req.session.user.id}] DOWNLOAD AUDIO[${audioData.id}]`,
      '',
      '',
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/audio/:id/like')
  async likeAudio(@Param('id', ParseIntPipe) audioId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);

    const audioData = await this.audioService.getByIdOrFail(audioId);
    const audioAuthor = await this.audioService.getAudioAuthor(audioId);
    let audioLikedBy = await this.audioService.getAudioLikedBy(audioId);

    const isLiked = await this.audioService.isLikedByUser(audioData.id, asUserData.id);
    if (!isLiked) {
      audioLikedBy.push(asUserData);
    } else {
      audioLikedBy = audioLikedBy.filter((user) => user.id !== asUserData.id);
    }

    const updatedAudio = await this.audioService.update({ ...audioData, likedBy: audioLikedBy });
    const isNowLiked: boolean = !isLiked;

    if (isNowLiked && asUserData.id !== audioAuthor.id) {
      await this.notificationsService.createNotification(
        `/audio/${audioId}`,
        `<b>${asUserData.username}</b> polubił plik <b>${audioData.title}</b>`,
        audioAuthor.id,
      );
    }

    await this.logsService.createLog(
      `USER[${asUserData.id}] LIKED AUDIO[${audioId}]`,
      ``,
      `${isNowLiked}`,
    );

    return {
      isLiked: isNowLiked,
      likeCount: updatedAudio.likedBy.length,
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Patch('/audio/:id')
  async editAudio(
    @Param('id', ParseIntPipe) audioId: number,
    @Req() req,
    @Body() body: EditAudioDto,
  ) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);

    const audioData = await this.audioService.getByIdOrFail(audioId);
    const audioAuthor = await this.audioService.getAudioAuthor(audioId);
    const beforeAudioData = { ...audioData };

    await this.permissionsService.validateAudioAction(
      ActionType.Edit,
      { ...audioData, author: audioAuthor },
      asUserData,
    );

    const isUserPremium = this.usersService.isUserPremium(asUserData);
    if (!isUserPremium && body.isPrivate) {
      throw new BadRequestException(
        'Nie możesz ustawić pliku jako prywatny. Opcja dostępna tylko dla członków premium.',
      );
    }

    if (!body.isPrivate && audioData.isPrivate) {
      audioData.authorizedUsers = [];
    }

    Object.assign(audioData, {
      imageUrl: validateImageUrl(body.imageUrl, isUserPremium),
      title: body.title,
      description: formatTextareaToString(body.description),
      category: body.category,
      bpm: body.bpm,
      key: body.key,
      keyType: body.keyType,
      isPrivate: body.isPrivate,
    });
    const editedAudioData = await this.audioService.update(audioData);

    await this.logsService.createLog(
      `USER[${req.session.user.id}] EDITED AUDIO[${editedAudioData.id}]`,
      `${JSON.stringify(beforeAudioData)}`,
      `${JSON.stringify(editedAudioData)}`,
    );

    return editedAudioData;
  }

  @UseGuards(AuthenticatedGuard)
  @Delete('/audio/:id')
  async deleteAudio(@Param('id', ParseIntPipe) audioId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);

    const audioData = await this.audioService.getByIdOrFail(audioId);
    const audioAuthor = await this.audioService.getAudioAuthor(audioId);

    await this.permissionsService.validateAudioAction(
      ActionType.Delete,
      { ...audioData, author: audioAuthor },
      asUserData,
    );

    const bucketName = this.configService.getOrThrow('S3_AUDIO_BUCKET_NAME');
    await this.storageService.deleteFile(audioData.fileKey, bucketName);
    await this.audioService.delete(audioId);

    await this.logsService.createLog(
      `USER[${req.session.user.id}] DELETED AUDIO[${audioId}]`,
      `${JSON.stringify(audioData)}`,
      '',
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/audio/:id/comments')
  async createComment(
    @Param('id', ParseIntPipe) audioId: number,
    @Body() body: CreateCommentDto,
    @Req() req,
  ) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    await this.permissionsService.validateCommentAction(
      ActionType.Create,
      {} as Comment,
      asUserData,
    );

    const audioData = await this.audioService.getByIdOrFail(audioId);
    const createdCommentData = await this.commentsService.create({
      author: asUserData,
      audioParent: audioData,
      content: formatTextareaToString(body.content),
      createdAt: new Date(),
    });

    const page = await this.commentsService.getCommentPageNumber(
      createdCommentData.audioParent.id,
      createdCommentData.id,
    );

    const audioAuthor = await this.audioService.getAudioAuthor(audioData.id);
    if (createdCommentData.author.id !== audioAuthor.id) {
      await this.notificationsService.createNotification(
        `/comments/${createdCommentData.id}`,
        `<b>${createdCommentData.author.username}</b> skomentował plik <b>${audioData.title}</b>`,
        audioAuthor.id,
      );
    }

    await this.logsService.createLog(
      `USER[${asUserData.id}] CREATED COMMENT[${createdCommentData.id}]`,
      ``,
      `${JSON.stringify(createdCommentData)}`,
    );

    return {
      ...createdCommentData,
      page,
    };
  }

  @UseGuards(AuthenticatedGuard)
  @Delete('/comments/:id')
  async deleteComment(@Param('id', ParseIntPipe) commentId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const commentData = await this.commentsService.getByIdOrFail(commentId, true);
    await this.permissionsService.validateCommentAction(ActionType.Delete, commentData, asUserData);

    await this.commentsService.delete(commentId);

    await this.logsService.createLog(
      `USER[${asUserData.id}] DELETED COMMENT[${commentData.id}]`,
      `${JSON.stringify(commentData)}`,
      '',
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/audio/:id/authorize/:username')
  async toggleAuthorizationUserForAudio(
    @Param('id', ParseIntPipe) audioId: number,
    @Req() req,
    @Param('username') username: string,
  ) {
    const audioData = await this.audioService.getByIdOrFail(audioId);
    if (!audioData.isPrivate) {
      throw new AudioNotPrivateException();
    }

    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const audioAuthor = await this.audioService.getAudioAuthor(audioId);
    await this.permissionsService.validateAudioAction(
      ActionType.Edit,
      { ...audioData, author: audioAuthor },
      asUserData,
    );

    const user = await this.usersService.getByUsernameOrFail(username);
    let authorizedUsers = await this.audioService.getAudioAuthorizedUsers(audioId);
    const isUserAuthorized = authorizedUsers.some(
      (authorizedUser) => authorizedUser.id === user.id,
    );

    if (isUserAuthorized) {
      authorizedUsers = authorizedUsers.filter((authorizedUser) => authorizedUser.id !== user.id);
    } else {
      authorizedUsers.push(user);
    }
    await this.audioService.update({ ...audioData, authorizedUsers });

    const isNowUserAuthorized = !isUserAuthorized;
    if (isNowUserAuthorized) {
      await this.notificationsService.createNotification(
        `/audio/${audioId}`,
        `<b>${asUserData.username}</b> przyznał Ci dostęp do pliku <b>${audioData.title}</b>`,
        user.id,
      );
    }

    await this.logsService.createLog(
      `USER[${asUserData.id}] TOGGLED ACCESS FOR USER[${user.id}] TO AUDIO[${audioId}]`,
      ``,
      `${isNowUserAuthorized}`,
    );
  }
}
