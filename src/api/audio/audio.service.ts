import { Injectable } from '@nestjs/common';
import { Audio } from './entity/audio.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entity/user.entity';
import { AudioNotFoundException } from '../../exceptions/audio/audio-not-found.exception';
import { SortBy } from 'src/enum/sortBy.enum';

@Injectable()
export class AudioService {
  constructor(
    @InjectRepository(Audio)
    private audioRepository: Repository<Audio>,
  ) {}

  async create(audioData: Omit<Audio, 'id'>): Promise<Audio> {
    return await this.audioRepository.save(audioData);
  }

  async getByIdOrFail(id: number): Promise<Audio> {
    const audioData = await this.audioRepository.findOne({
      where: { id },
    });

    if (!audioData) {
      throw new AudioNotFoundException();
    }

    return audioData;
  }

  async getByQuery(
    query: Record<string, any>,
    getPrivate = false,
    page = 1,
    pageSize = 20,
  ): Promise<{ results: Audio[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Audio> = this.audioRepository.createQueryBuilder('audio');

    Object.entries(query).forEach(([key, value]) => {
      switch (key) {
        case 'keywords':
          queryBuilder = queryBuilder.andWhere('audio.title LIKE :keywords', {
            keywords: `%${value}%`,
          });
          break;
        case 'minBpm':
          queryBuilder = queryBuilder.andWhere('audio.bpm >= :minBpm', { minBpm: value });
          break;
        case 'maxBpm':
          queryBuilder = queryBuilder.andWhere('audio.bpm <= :maxBpm', { maxBpm: value });
          break;
        case 'key':
          queryBuilder = queryBuilder.andWhere('audio.key = :key', { key: value });
          break;
        case 'keyType':
          queryBuilder = queryBuilder.andWhere('audio.keyType = :keyType', { keyType: value });
          break;
        case 'category':
          queryBuilder = queryBuilder.andWhere('audio.category = :category', { category: value });
          break;
      }
    });

    switch (query.sortBy) {
      case SortBy.Newest:
        queryBuilder = queryBuilder.orderBy('audio.createdAt', 'DESC');
        break;
      case SortBy.Oldest:
        queryBuilder = queryBuilder.orderBy('audio.createdAt', 'ASC');
        break;
      default:
        queryBuilder = queryBuilder.orderBy('audio.createdAt', 'DESC');
        break;
    }

    if (!getPrivate) queryBuilder = queryBuilder.andWhere('audio.isPrivate == FALSE');
    queryBuilder.skip((page - 1) * pageSize).take(pageSize);

    const [audio, count] = await queryBuilder.getManyAndCount();
    const totalPages: number = Math.ceil(count / pageSize);

    return { results: audio, totalPages };
  }

  async update(audioData: Audio): Promise<Audio> {
    return await this.audioRepository.save(audioData);
  }

  async delete(audioId: number): Promise<void> {
    const audio = await this.getByIdOrFail(audioId);
    await this.audioRepository.remove(audio);
  }

  /* RELATIONS */
  async getAudioAuthor(audioId: number): Promise<User> {
    const user: User = await this.audioRepository
      .createQueryBuilder('audio')
      .relation(Audio, 'author')
      .of(audioId)
      .loadOne();

    return user;
  }

  async getAudioLikedBy(audioId: number): Promise<User[]> {
    const users: User[] = await this.audioRepository
      .createQueryBuilder('audio')
      .relation(Audio, 'likedBy')
      .of(audioId)
      .loadMany();

    return users || [];
  }

  async getAudioAuthorizedUsers(audioId: number): Promise<User[]> {
    return await this.audioRepository
      .createQueryBuilder('audio')
      .relation(Audio, 'authorizedUsers')
      .of(audioId)
      .loadMany();
  }

  /* HELPERS */
  async getAudioByUser(
    userId: number,
    getPrivate = false,
    page = 1,
    pageSize = 20,
  ): Promise<{ audioData: Audio[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Audio> = this.audioRepository.createQueryBuilder('audio');

    queryBuilder = queryBuilder.where('audio.authorId = :userId', { userId });

    if (!getPrivate) {
      queryBuilder = queryBuilder.andWhere('audio.isPrivate == FALSE');
    }

    queryBuilder = queryBuilder
      .orderBy('audio.createdAt', 'DESC')
      .skip((page - 1) * pageSize)
      .take(pageSize);

    const [audio, count] = await queryBuilder.getManyAndCount();

    return { audioData: audio, totalPages: Math.ceil(count / pageSize) };
  }

  async getAudioLikesCount(audioId: number): Promise<number> {
    const query = `
    SELECT COUNT(*) AS likesCount
    FROM user_likedAudio
    WHERE file_id = ${audioId};
  `;

    const result = await this.audioRepository.query(query);
    return parseInt(result[0].likesCount);
  }

  async isLikedByUser(audioId: number, userId: number): Promise<boolean> {
    const likedBy = await this.getAudioLikedBy(audioId);
    return likedBy.some((user) => user.id === userId);
  }
}
