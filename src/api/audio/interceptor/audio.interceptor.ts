import { Injectable } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileWrongMimetypeException } from 'src/exceptions/files/file-wrong-mimetype.exception';

@Injectable()
export class AudioFileInterceptor extends FileInterceptor('audioFile', {
  limits: {
    files: 1,
    fileSize: 30 * 1024 * 1024,
  },
  fileFilter: (req, file, callback) => {
    if (file.mimetype !== 'audio/wav' && file.mimetype !== 'audio/mpeg') {
      callback(new FileWrongMimetypeException(), false);
    }

    callback(null, true);
  },
}) {}

export default AudioFileInterceptor;
