import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class BanUserDto {
  @IsString()
  @MaxLength(200, {
    message: 'Powód nie może być dłuższy niż 200 znaków.',
  })
  @IsNotEmpty({ message: 'Powód nie może być pusty.' })
  reason: string;
}
