import {
  IsAlphanumeric,
  IsBoolean,
  IsDateString,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class ManageUserDto {
  @IsString()
  @MaxLength(300, {
    message: 'URL zdjęcia nie może być dłuższy niż 300 znaków.',
  })
  avatarUrl: string;

  @IsAlphanumeric('pl-Pl', {
    message: 'Nazwa użytkownika może zawierać tylko litery i cyfry.',
  })
  @MinLength(3, {
    message: 'Nazwa użytkownika nie może być krótsza niż 3 znaki.',
  })
  @MaxLength(16, {
    message: 'Nazwa użytkownika nie może być dłuższa niż 16 znaków.',
  })
  username: string;

  @IsString()
  @MaxLength(300, {
    message: 'Opis nie może być dłuższy niż 300 znaków.',
  })
  aboutMe: string;

  @IsBoolean()
  showDiscordProfile: boolean;

  @IsBoolean()
  showLastOnline: boolean;

  @IsString()
  @MaxLength(300, {
    message: 'Link do profilu Instagram nie może być dłuższy nić 300 znaków.',
  })
  instagramUrl: string;

  @IsString()
  @MaxLength(300, {
    message: 'Link do profilu SoundCloud nie może być dłuższy niż 300 znaków.',
  })
  soundCloudUrl: string;

  @IsString()
  @MaxLength(300, {
    message: 'Link do profilu YouTube nie może być dłuższy niż 300 znaków.',
  })
  youtubeUrl: string;

  @IsString()
  @MaxLength(300, {
    message: 'Link do profilu Facebook nie może być dłuższy niż 300 znaków.',
  })
  facebookUrl: string;

  @IsString()
  @MaxLength(300, {
    message: 'Link do profilu Spotify nie może być dłuższy niż 300 znaków.',
  })
  spotifyUrl: string;

  @IsBoolean()
  isUserAdmin: boolean;

  @IsBoolean()
  isUserModerator: boolean;

  @IsBoolean()
  verified: boolean;

  @IsDateString(
    { strict: true },
    { message: 'Nieprawidłowa data wygaśnięcia członkostwa Premium.' },
  )
  premiumPlanExpiration: Date;
}
