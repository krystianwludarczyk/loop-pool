import { Injectable } from '@nestjs/common';
import { User } from './entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { UserNotFoundException } from '../../exceptions/users/user-not-found.exception';
import { SortBy } from 'src/enum/sortBy.enum';
import { Offer } from '../offers/entity/offer.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(userData: Omit<User, 'id'>): Promise<User> {
    return await this.usersRepository.save(userData);
  }

  async getUsersCount(): Promise<number> {
    return await this.usersRepository.count();
  }

  async getByUsername(username: string): Promise<User | undefined> {
    const lowercaseUsername = username.toLowerCase();
    return await this.usersRepository
      .createQueryBuilder('user')
      .where('LOWER(user.username) = :lowercaseUsername', { lowercaseUsername })
      .getOne();
  }

  async getByUsernameOrFail(username: string): Promise<User> {
    const lowercaseUsername = username.toLowerCase();
    const user = await this.usersRepository
      .createQueryBuilder('user')
      .where('LOWER(user.username) = :lowercaseUsername', { lowercaseUsername })
      .getOne();

    if (!user) {
      throw new UserNotFoundException();
    }

    return user;
  }

  async getById(userId: number): Promise<User | undefined> {
    return await this.usersRepository.findOne({
      where: { id: userId },
    });
  }

  async getByIdOrFail(userId: number): Promise<User> {
    const userData = await this.usersRepository.findOne({
      where: { id: userId },
    });

    if (!userData) {
      throw new UserNotFoundException();
    }

    return userData;
  }

  async getByDiscordId(discordId: number): Promise<User | undefined> {
    return await this.usersRepository.findOneBy({ discordId: discordId.toString() });
  }

  async getByQuery(
    query: Record<string, any>,
    page = 1,
    pageSize = 20,
  ): Promise<{ usersData: User[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<User> = this.usersRepository.createQueryBuilder('user');

    Object.entries(query).forEach(([key, value]) => {
      switch (key) {
        case 'username':
          queryBuilder = queryBuilder.andWhere('user.username LIKE :username', {
            username: `%${value}%`,
          });
          break;
      }
    });

    switch (query.sortBy) {
      case SortBy.AZ:
        queryBuilder = queryBuilder.orderBy('LOWER(user.username)', 'ASC');
        break;
      case SortBy.ZA:
        queryBuilder = queryBuilder.orderBy('LOWER(user.username)', 'DESC');
        break;
      default:
        queryBuilder = queryBuilder.orderBy('LOWER(user.username)', 'ASC');
        break;
    }

    queryBuilder.skip((page - 1) * pageSize).take(pageSize);

    const [usersData, count] = await queryBuilder.getManyAndCount();
    const totalPages: number = Math.ceil(count / pageSize);

    return { usersData, totalPages };
  }

  async update(userData: User): Promise<User> {
    return await this.usersRepository.save(userData);
  }

  /* RELATIONS */
  async getUserFollowers(userId: number): Promise<User[]> {
    return await this.usersRepository
      .createQueryBuilder('user')
      .relation(User, 'followers')
      .of(userId)
      .loadMany();
  }

  async getUserFollowing(userId: number): Promise<User[]> {
    return await this.usersRepository
      .createQueryBuilder('user')
      .relation(User, 'following')
      .of(userId)
      .loadMany();
  }

  async getUserOffer(userId: number): Promise<Offer> {
    return await this.usersRepository
      .createQueryBuilder('user')
      .relation(User, 'offer')
      .of(userId)
      .loadOne();
  }

  /* HELPERS */
  isUserPremium(userData: User): boolean {
    return userData.premiumPlanExpiration > new Date();
  }

  async isUserFollower(userId: number, asUserId: number): Promise<boolean> {
    const userFollowers = await this.getUserFollowers(userId);
    return userFollowers.some((follower) => follower.id === asUserId);
  }

  async getUserFollowersCount(userId: number): Promise<number> {
    const query = `
    SELECT COUNT(*) AS followersCount
    FROM user_followers
    WHERE user_id = ${userId};
  `;

    const result = await this.usersRepository.query(query);
    return parseInt(result[0].followersCount);
  }

  async getUserFollowingCount(userId: number): Promise<number> {
    const query = `
    SELECT COUNT(*) AS followingCount
    FROM user_followers
    WHERE follower_id = ${userId};
  `;

    const result = await this.usersRepository.query(query);
    return parseInt(result[0].followingCount);
  }
}
