import {
  Controller,
  Param,
  UseGuards,
  Req,
  Body,
  Post,
  ParseIntPipe,
  Patch,
  BadRequestException,
  Put,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthenticatedGuard } from 'src/api/auth/guard/authenticated.guard';
import { EditUserDto } from './dto/edit-user.dto';
import { formatTextareaToString, formatUsername } from 'src/helpers/format.helper';
import { LogsService } from 'src/services/logger/logs.service';
import { NotificationsService } from 'src/services/notifications/notifications.service';
import {
  validateFacebookUrl,
  validateImageUrl,
  validateInstagramUrl,
  validateSoundCloudUrl,
  validateSpotifyUrl,
  validateYouTubeUrl,
} from 'src/helpers/validator.helper';
import { PermissionsService } from 'src/services/permissions/permissions.service';
import { ActionType } from 'src/services/permissions/action.enum';
import { ManageUserDto } from './dto/manage-user.dto';
import { BanUserDto } from './dto/ban-user.dto';
import { MuteUserDto } from './dto/mute-user.dto';
import { UserNameExistsException } from '../../exceptions/users/user-name-exists.exception';
import { UserFollowSelfException } from 'src/exceptions/users/user-follow-self.exception';

@Controller()
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly notificationsService: NotificationsService,
    private readonly logsService: LogsService,
    private readonly permissionsService: PermissionsService,
  ) {}

  @UseGuards(AuthenticatedGuard)
  @Post('/users/:id/follow')
  async followUser(@Param('id', ParseIntPipe) userId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);

    if (asUserData.id === userId) {
      throw new UserFollowSelfException();
    }

    const userData = await this.usersService.getByIdOrFail(userId);
    let userFollowers = await this.usersService.getUserFollowers(userId);
    const isAlreadyFollowing = await this.usersService.isUserFollower(userId, asUserData.id);

    if (isAlreadyFollowing) {
      userFollowers = userFollowers.filter((user) => user.id !== asUserData.id);
    } else {
      userFollowers.push(asUserData);
    }
    const updatedUser = await this.usersService.update({ ...userData, followers: userFollowers });

    const isNowFollowing = !isAlreadyFollowing;
    await this.logsService.createLog(
      `USER[${req.session.user.id}] FOLLOWED USER[${userId}]`,
      ``,
      `${isNowFollowing}`,
    );

    if (isNowFollowing) {
      await this.notificationsService.createNotification(
        `/users/${asUserData.id}`,
        `<b>${asUserData.username}</b> obserwuje Cię`,
        userId,
      );
    }
    return { status: isNowFollowing, followersCount: updatedUser.followers.length };
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/users/:id/mute')
  async muteUser(@Param('id', ParseIntPipe) userId: number, @Body() body: MuteUserDto, @Req() req) {
    const userData = await this.usersService.getByIdOrFail(userId);
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);

    await this.permissionsService.validateUserAction(ActionType.Mute, userData, asUserData);

    userData.muteReason = body.reason;
    const editedUserData = await this.usersService.update(userData);

    await this.logsService.createLog(
      `USER[${asUserData.id}] MUTED USER[${userData.id}]`,
      ``,
      `${editedUserData.muteReason}`,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/users/:id/unmute')
  async unmuteUser(@Param('id', ParseIntPipe) userId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const userData = await this.usersService.getByIdOrFail(userId);
    const beforeUserData = await this.usersService.getByIdOrFail(userId);

    await this.permissionsService.validateUserAction(ActionType.Mute, userData, asUserData);

    userData.muteReason = null;
    await this.usersService.update(userData);

    await this.logsService.createLog(
      `USER[${asUserData.id}] UNMUTED USER[${userData.id}]`,
      `${beforeUserData.muteReason}`,
      ``,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/users/:id/ban')
  async banUser(@Param('id', ParseIntPipe) userId: number, @Body() body: BanUserDto, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const userData = await this.usersService.getByIdOrFail(userId);

    await this.permissionsService.validateUserAction(ActionType.Ban, userData, asUserData);

    userData.banReason = body.reason;
    const editedUserData = await this.usersService.update(userData);

    await this.logsService.createLog(
      `USER[${asUserData.id}] BANNED USER[${userData.id}]`,
      ``,
      `${editedUserData.banReason}`,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/users/:id/unban')
  async unbanUser(@Param('id', ParseIntPipe) userId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const userData = await this.usersService.getByIdOrFail(userId);
    const beforeUserData = await this.usersService.getByIdOrFail(userId);

    await this.permissionsService.validateUserAction(ActionType.Ban, userData, asUserData);

    userData.banReason = null;
    await this.usersService.update(userData);

    await this.logsService.createLog(
      `USER[${asUserData.id}] UNBANNED USER[${userData.id}]`,
      `${beforeUserData.banReason}`,
      ``,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Patch('/users/:id')
  async editUser(@Param('id', ParseIntPipe) userId: number, @Body() body: EditUserDto, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const userData = await this.usersService.getByIdOrFail(userId);
    const beforeUserData = await this.usersService.getByIdOrFail(userId);

    await this.permissionsService.validateUserAction(ActionType.Edit, userData, asUserData);
    const usernameExists = await this.usersService.getByUsername(body.username);
    if (usernameExists && userData.id !== usernameExists.id) {
      throw new UserNameExistsException();
    }

    const isUserPremium = this.usersService.isUserPremium(userData);
    Object.assign(userData, {
      ...body,
      aboutMe: formatTextareaToString(body.aboutMe),
      avatarUrl: validateImageUrl(body.avatarUrl, isUserPremium),
      instagramUrl: validateInstagramUrl(body.instagramUrl),
      soundCloudUrl: validateSoundCloudUrl(body.soundCloudUrl),
      youtubeUrl: validateYouTubeUrl(body.youtubeUrl),
      facebookUrl: validateFacebookUrl(body.facebookUrl),
      spotifyUrl: validateSpotifyUrl(body.spotifyUrl),
    });

    const editedUserData = await this.usersService.update(userData);

    const afterUserData = await this.usersService.getByIdOrFail(editedUserData.id);
    await this.logsService.createLog(
      `USER[${req.session.user.id}] EDITED USER[${editedUserData.id}]`,
      `${JSON.stringify(beforeUserData)}`,
      `${JSON.stringify(afterUserData)}`,
    );

    return editedUserData;
  }

  @Put('/users/:id')
  async manageUser(
    @Param('id', ParseIntPipe) userId: number,
    @Body() body: ManageUserDto,
    @Req() req,
  ) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const userData = await this.usersService.getByIdOrFail(userId);
    const beforeUserData = await this.usersService.getByIdOrFail(userId);

    await this.permissionsService.validateUserAction(ActionType.Manage, userData, asUserData);
    const usernameExists = await this.usersService.getByUsername(body.username);
    if (usernameExists && userData.id !== usernameExists.id) {
      throw new UserNameExistsException();
    }

    Object.assign(userData, {
      ...body,
      aboutMe: formatTextareaToString(body.aboutMe),
      avatarUrl: validateImageUrl(body.avatarUrl, true),
      instagramUrl: validateInstagramUrl(body.instagramUrl),
      soundCloudUrl: validateSoundCloudUrl(body.soundCloudUrl),
      youtubeUrl: validateYouTubeUrl(body.youtubeUrl),
      facebookUrl: validateFacebookUrl(body.facebookUrl),
      spotifyUrl: validateSpotifyUrl(body.spotifyUrl),
    });
    const editedUserData = await this.usersService.update(userData);

    const afterUserData = await this.usersService.getByIdOrFail(editedUserData.id);
    await this.logsService.createLog(
      `USER[${req.session.user.id}] MANAGED USER[${editedUserData.id}]`,
      `${JSON.stringify(beforeUserData)}`,
      `${JSON.stringify(afterUserData)}`,
    );

    return editedUserData;
  }
}
