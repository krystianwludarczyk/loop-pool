import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './entity/user.entity';
import { LogsModule } from 'src/services/logger/logs.module';
import { NotificationsModule } from 'src/services/notifications/notifications.module';
import { PermissionsModule } from 'src/services/permissions/permissions.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), LogsModule, NotificationsModule, PermissionsModule],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
