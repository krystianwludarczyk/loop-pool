import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Audio } from '../../audio/entity/audio.entity';
import { Comment } from 'src/api/audio/entity/comment.entity';
import { Topic } from 'src/api/topics/entity/topic.entity';
import { Transaction } from 'src/api/transactions/entity/transaction.entity';
import { Reply } from 'src/api/topics/entity/reply.entity';
import { Offer } from 'src/api/offers/entity/offer.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  discordId: string;

  @Column()
  showDiscordProfile: boolean;

  @Column()
  email: string;

  @Column()
  isUserAdmin: boolean;

  @Column()
  isUserModerator: boolean;

  @Column({ nullable: true })
  muteReason: string;

  @Column({ nullable: true })
  banReason: string;

  @Column()
  registredAt: Date;

  @Column()
  lastOnline: Date;

  @Column()
  showLastOnline: boolean;

  @Column({ nullable: true })
  instagramUrl: string;

  @Column({ nullable: true })
  soundCloudUrl: string;

  @Column({ nullable: true })
  youtubeUrl: string;

  @Column({ nullable: true })
  facebookUrl: string;

  @Column({ nullable: true })
  spotifyUrl: string;

  @Column({ nullable: true })
  avatarUrl: string;

  @Column({ nullable: true })
  aboutMe: string;

  @Column({ default: false })
  verified: boolean;

  @Column()
  premiumPlanExpiration: Date;

  @OneToMany(() => Audio, (audio) => audio.author)
  audio: Audio[];

  @ManyToMany(() => User, (user) => user.followers)
  following: User[];

  @ManyToMany(() => User, (user) => user.following)
  @JoinTable({
    name: 'user_followers',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'follower_id', referencedColumnName: 'id' },
  })
  followers: User[];

  @ManyToMany(() => Audio, (audio) => audio.likedBy)
  @JoinTable({
    name: 'user_likedAudio',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'file_id', referencedColumnName: 'id' },
  })
  likedAudio: Audio[];

  @OneToMany(() => Comment, (comment) => comment.author)
  comments: Comment[];

  @OneToMany(() => Topic, (topic) => topic.author)
  topics: Topic[];

  @OneToOne(() => Offer, (offer) => offer.author, { nullable: true, onDelete: 'SET NULL' })
  @JoinColumn()
  offer: Offer;

  @OneToMany(() => Reply, (reply) => reply.author)
  replies: Reply[];

  @ManyToMany(() => Topic, (topic) => topic.followedBy)
  @JoinTable({
    name: 'user_followingTopics',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'topic_id', referencedColumnName: 'id' },
  })
  followingTopics: Topic[];

  @OneToMany(() => Transaction, (transaction) => transaction.user)
  transactions: Transaction[];

  @ManyToMany(() => Audio, (audio) => audio.authorizedUsers)
  @JoinTable({
    name: 'user_allowedAudio',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'file_id', referencedColumnName: 'id' },
  })
  allowedAudio: Audio[];
}
