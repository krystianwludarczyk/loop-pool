import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class EditTopicDto {
  @IsNotEmpty({ message: 'Tytuł nie może być pusty.' })
  @IsString()
  @MaxLength(100, { message: 'Tytuł nie może być dłuższy niż 1000 znaków.' })
  title: string;

  @IsNotEmpty({ message: 'Treść nie może być pusta.' })
  @IsString()
  @MaxLength(1000, { message: 'Treść nie może być dłuższa niż 1000 znaków.' })
  content: string;
}
