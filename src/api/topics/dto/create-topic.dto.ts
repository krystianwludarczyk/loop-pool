import { IsBoolean, IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateTopicDto {
  @IsNotEmpty({ message: 'Tytuł nie może być pusty.' })
  @IsString()
  @MaxLength(100, { message: 'Tytuł nie może być dłuższy niż 100 znaków.' })
  title: string;

  @IsNotEmpty({ message: 'Treść nie może być pusta.' })
  @IsString()
  @MaxLength(1000, { message: 'Treść nie może być dłuższa niż 1000 znaków.' })
  content: string;

  @IsBoolean()
  follow: boolean;
}
