import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateReplyDto {
  @IsNotEmpty({ message: 'Treść nie może być pusta.' })
  @IsString()
  @MaxLength(1000, { message: 'Treść nie może być dłuższa niż 1000 znaków.' })
  content: string;
}
