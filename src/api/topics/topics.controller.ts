import {
  Controller,
  Req,
  Body,
  Post,
  UseGuards,
  Delete,
  Param,
  ParseIntPipe,
  Patch,
} from '@nestjs/common';
import { AuthenticatedGuard } from '../auth/guard/authenticated.guard';
import { LogsService } from 'src/services/logger/logs.service';
import { UsersService } from '../users/users.service';
import { formatTextareaToString } from 'src/helpers/format.helper';
import { NotificationsService } from 'src/services/notifications/notifications.service';
import { PermissionsService } from 'src/services/permissions/permissions.service';
import { TopicsService } from './topics.service';
import { CreateTopicDto } from './dto/create-topic.dto';
import { ActionType } from 'src/services/permissions/action.enum';
import { Topic } from './entity/topic.entity';
import { EditTopicDto } from './dto/edit-topic.dto';
import { CreateReplyDto } from './dto/create-reply.dto';
import { Reply } from './entity/reply.entity';
import { RepliesService } from './replies.service';

@Controller()
export class TopicsController {
  constructor(
    private readonly topicsService: TopicsService,
    private readonly repliesService: RepliesService,
    private readonly logsService: LogsService,
    private readonly notificationsService: NotificationsService,
    private readonly usersService: UsersService,
    private readonly permissionsService: PermissionsService,
  ) {}

  @UseGuards(AuthenticatedGuard)
  @Post('/topics')
  async createTopic(@Body() body: CreateTopicDto, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    await this.permissionsService.validateTopicAction(ActionType.Create, {} as Topic, asUserData);

    const followers = [];
    if (body.follow) {
      followers.push(asUserData);
    }

    const createdTopicData = await this.topicsService.create({
      author: asUserData,
      title: body.title,
      content: formatTextareaToString(body.content),
      createdAt: new Date(),
      lastUpdated: new Date(),
      followedBy: followers,
      replies: [],
      isPinned: false,
    });

    const userFollowers = await this.usersService.getUserFollowers(asUserData.id);
    for (const follower of userFollowers) {
      await this.notificationsService.createNotification(
        `/topics/${createdTopicData.id}`,
        `<b>${createdTopicData.author.username}</b> opublikował temat <b>${createdTopicData.title}</b>`,
        follower.id,
      );
    }

    const afterTopicData = await this.topicsService.getByIdOrFail(createdTopicData.id);
    await this.logsService.createLog(
      `USER[${asUserData.id}] CREATED TOPIC[${createdTopicData.id}]`,
      ``,
      `${JSON.stringify(afterTopicData)}`,
    );

    return createdTopicData;
  }

  @UseGuards(AuthenticatedGuard)
  @Patch('/topics/:id')
  async editTopic(
    @Param('id', ParseIntPipe) topicId: number,
    @Body() body: EditTopicDto,
    @Req() req,
  ) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const topicData = await this.topicsService.getByIdOrFail(topicId);
    const author = await this.topicsService.getTopicAuthor(topicId);
    const beforeEditTopicData = await this.topicsService.getByIdOrFail(topicId);

    await this.permissionsService.validateTopicAction(
      ActionType.Edit,
      { ...topicData, author },
      asUserData,
    );

    Object.assign(topicData, {
      title: body.title,
      content: formatTextareaToString(body.content),
    });
    const editedTopicData = await this.topicsService.update(topicData);

    const afterTopicData = await this.topicsService.getByIdOrFail(editedTopicData.id);
    await this.logsService.createLog(
      `USER[${asUserData.id}] EDITED TOPIC[${editedTopicData.id}]`,
      `${JSON.stringify(beforeEditTopicData)}`,
      `${JSON.stringify(afterTopicData)}`,
    );

    return editedTopicData;
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/topics/:id/pin')
  async pinTopic(@Param('id', ParseIntPipe) topicId: number, @Req() req) {
    const asUserData = await this.usersService.getById(req.session.user.id);
    const topicData = await this.topicsService.getByIdOrFail(topicId);
    await this.permissionsService.validateTopicAction(ActionType.Pin, topicData, asUserData);

    topicData.isPinned = !topicData.isPinned;
    await this.topicsService.update(topicData);

    if (topicData.isPinned) {
      const followedBy = await this.topicsService.getTopicFollowedBy(topicId);
      for (const follower of followedBy) {
        await this.notificationsService.createNotification(
          `/topics/${topicData.id}`,
          `<b>${asUserData.username}</b> przypiął temat <b>${topicData.title}</b>`,
          follower.id,
        );
      }
    }

    await this.logsService.createLog(
      `USER[${asUserData.id}] PINNED TOPIC[${topicId}]`,
      ``,
      `${topicData.isPinned}`,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Post('/topics/:id/follow')
  async followTopic(@Param('id', ParseIntPipe) topicId: number, @Req() req) {
    const asUserData = await this.usersService.getById(req.session.user.id);

    const topicData = await this.topicsService.getByIdOrFail(topicId);
    const isAlreadyFollowing = await this.topicsService.isTopicFollowedByUser(
      topicId,
      asUserData.id,
    );
    let followedBy = await this.topicsService.getTopicFollowedBy(topicId);

    if (isAlreadyFollowing) {
      followedBy = followedBy.filter((user) => user.id !== asUserData.id);
    } else {
      followedBy.push(asUserData);
    }

    await this.topicsService.update({ ...topicData, followedBy: followedBy });

    const isNowFollowing = !isAlreadyFollowing;
    await this.logsService.createLog(
      `USER[${asUserData.id}] FOLLOWED TOPIC[${topicId}]`,
      ``,
      `${isNowFollowing}`,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Delete('/topics/:id')
  async deleteTopic(@Param('id', ParseIntPipe) topicId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const topicData = await this.topicsService.getByIdOrFail(topicId);
    const author = await this.topicsService.getTopicAuthor(topicId);

    await this.permissionsService.validateTopicAction(
      ActionType.Delete,
      { ...topicData, author },
      asUserData,
    );

    await this.topicsService.delete(topicId);

    await this.logsService.createLog(
      `USER[${asUserData.id}] DELETED TOPIC[${topicData.id}]`,
      `${JSON.stringify(topicData)}`,
      ``,
    );
  }

  /* REPLIES*/
  @UseGuards(AuthenticatedGuard)
  @Post('/topics/:id/replies')
  async createReply(
    @Param('id', ParseIntPipe) topicId: number,
    @Body() body: CreateReplyDto,
    @Req() req,
  ) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    await this.permissionsService.validateReplyAction(ActionType.Create, {} as Reply, asUserData);

    const topicData = await this.topicsService.getByIdOrFail(topicId);
    const now = new Date();
    topicData.lastUpdated = now;
    await this.topicsService.update(topicData);

    const createdReplyData = await this.repliesService.create({
      author: asUserData,
      topicParent: topicData,
      content: formatTextareaToString(body.content),
      createdAt: now,
    });

    const followedBy = await this.topicsService.getTopicFollowedBy(topicId);
    for (const follower of followedBy) {
      if (follower.id !== createdReplyData.author.id) {
        await this.notificationsService.createNotification(
          `/replies/${createdReplyData.id}`,
          `<b>${createdReplyData.author.username}</b> dodał odpowiedź w temacie <b>${topicData.title}</b>`,
          follower.id,
        );
      }
    }

    await this.logsService.createLog(
      `USER[${asUserData.id}] CREATED REPLY[${createdReplyData.id}]`,
      ``,
      `${JSON.stringify(createdReplyData)}`,
    );

    return createdReplyData;
  }

  @UseGuards(AuthenticatedGuard)
  @Delete('/replies/:id')
  async deleteReply(@Param('id', ParseIntPipe) replyId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const replyData = await this.repliesService.getByIdOrFail(replyId, true);

    await this.permissionsService.validateReplyAction(ActionType.Delete, replyData, asUserData);

    await this.repliesService.delete(replyId);
    await this.logsService.createLog(
      `USER[${asUserData.id}] DELETED REPLY[${replyData.id}]`,
      `${JSON.stringify(replyData)}`,
      ``,
    );
  }
}
