import { Injectable } from '@nestjs/common';
import { Topic } from './entity/topic.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entity/user.entity';
import { TopicNotFoundException } from '../../exceptions/topics/topic-not-found.exception';

@Injectable()
export class TopicsService {
  constructor(
    @InjectRepository(Topic)
    private topicsRepository: Repository<Topic>,
  ) {}

  async create(topicData: Omit<Topic, 'id'>): Promise<Topic> {
    return await this.topicsRepository.save(topicData);
  }

  async getByIdOrFail(topicId: number): Promise<Topic> {
    const topic = await this.topicsRepository.findOne({
      where: { id: topicId },
    });

    if (!topic) {
      throw new TopicNotFoundException();
    }

    return topic;
  }

  async getByQuery(
    page = 1,
    pageSize = 20,
    keywords?: string,
    loadRelations = false,
  ): Promise<{ topicsData: Topic[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Topic> = this.topicsRepository.createQueryBuilder('topic');

    if (loadRelations) {
      queryBuilder = queryBuilder.leftJoinAndSelect('topic.author', 'author');
      queryBuilder = queryBuilder.leftJoinAndSelect('topic.replies', 'replies');
      queryBuilder = queryBuilder.leftJoinAndSelect('topic.followedBy', 'followedBy');
    }

    queryBuilder = queryBuilder
      .orderBy('topic.isPinned', 'DESC')
      .addOrderBy('topic.lastUpdated', 'DESC');

    if (keywords) {
      queryBuilder = queryBuilder.andWhere('topic.title LIKE :keywords', {
        keywords: `%${keywords}%`,
      });
    }

    const totalResults: number = await queryBuilder.getCount();
    const totalPages: number = Math.ceil(totalResults / pageSize);
    queryBuilder.skip((page - 1) * pageSize).take(pageSize);
    const topicsData: Topic[] = await queryBuilder.getMany();

    return { topicsData, totalPages };
  }

  async update(topicData: Topic): Promise<Topic> {
    return await this.topicsRepository.save(topicData);
  }

  async delete(topicId: number): Promise<void> {
    const topicData = await this.getByIdOrFail(topicId);
    await this.topicsRepository.remove(topicData);
  }

  /* RELATIONS */
  async getTopicAuthor(topicId: number): Promise<User> {
    const user: User = await this.topicsRepository
      .createQueryBuilder('topic')
      .relation(Topic, 'author')
      .of(topicId)
      .loadOne();

    return user;
  }

  async getTopicFollowedBy(topicId: number): Promise<User[]> {
    const users: User[] = await this.topicsRepository
      .createQueryBuilder('topic')
      .relation(Topic, 'followedBy')
      .of(topicId)
      .loadMany();

    return users;
  }

  /* HELPERS */
  async isTopicFollowedByUser(topicId: number, userId: number): Promise<boolean> {
    const followedBy = await this.getTopicFollowedBy(topicId);
    return followedBy.some((follower) => follower.id === userId);
  }
}
