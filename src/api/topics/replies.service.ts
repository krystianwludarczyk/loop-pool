import { Injectable } from '@nestjs/common';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Reply } from './entity/reply.entity';
import { SortBy } from '../../enum/sortBy.enum';
import { ReplyNotFoundException } from '../../exceptions/replies/reply-not-found.exception';

@Injectable()
export class RepliesService {
  constructor(
    @InjectRepository(Reply)
    private repliesRepository: Repository<Reply>,
  ) {}

  async create(replyData: Omit<Reply, 'id'>): Promise<Reply> {
    return await this.repliesRepository.save(replyData);
  }

  async getByIdOrFail(replyId: number, loadRelations = false): Promise<Reply> {
    const reply = await this.repliesRepository.findOne({
      where: { id: replyId },
      relations: loadRelations ? ['author', 'topicParent'] : null,
    });

    if (!reply) {
      throw new ReplyNotFoundException();
    }

    return reply;
  }

  async getByQuery(
    query: Record<string, any>,
    page = 1,
    pageSize = 20,
    loadRelations = false,
  ): Promise<{ repliesData: Reply[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Reply> =
      this.repliesRepository.createQueryBuilder('reply');

    if (loadRelations) {
      queryBuilder = queryBuilder
        .leftJoinAndSelect('reply.author', 'author')
        .leftJoinAndSelect('reply.topicParent', 'topicParent');
    }

    switch (query.sortBy) {
      case SortBy.Newest:
        queryBuilder = queryBuilder.orderBy('reply.createdAt', 'DESC');
        break;
      case SortBy.Oldest:
        queryBuilder = queryBuilder.orderBy('reply.createdAt', 'ASC');
        break;
      default:
        queryBuilder = queryBuilder.orderBy('reply.createdAt', 'DESC');
        break;
    }

    Object.entries(query).forEach(([key, value]) => {
      switch (key) {
        case 'keywords':
          queryBuilder = queryBuilder.andWhere('reply.content LIKE :keywords', {
            keywords: `%${value}%`,
          });
          break;
      }
    });

    const totalResults: number = await queryBuilder.getCount();
    const totalPages: number = Math.ceil(totalResults / pageSize);
    queryBuilder.skip((page - 1) * pageSize).take(pageSize);
    const repliesData: Reply[] = await queryBuilder.getMany();

    return { repliesData, totalPages };
  }

  async delete(replyId: number): Promise<void> {
    const replyData = await this.getByIdOrFail(replyId, true);
    await this.repliesRepository.remove(replyData);
  }

  /* HELPERS */
  async getRepliesByTopic(
    topicId: number,
    page = 1,
    pageSize = 20,
    loadRelations = false,
  ): Promise<{ repliesData: Reply[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Reply> =
      this.repliesRepository.createQueryBuilder('reply');

    if (loadRelations) {
      queryBuilder = queryBuilder
        .leftJoinAndSelect('reply.author', 'author')
        .leftJoinAndSelect('reply.topicParent', 'topicParent');
    }

    queryBuilder = queryBuilder
      .where('reply.topicParentId = :topicId', { topicId })
      .orderBy('reply.createdAt', 'ASC')
      .skip((page - 1) * pageSize)
      .take(pageSize);

    const [replies, count] = await queryBuilder.getManyAndCount();

    return { repliesData: replies, totalPages: Math.ceil(count / pageSize) };
  }

  async getReplyPageNumber(topicId: number, replyId: number, pageSize = 20): Promise<number> {
    const reply = await this.repliesRepository.exists({ where: { id: replyId } });
    if (!reply) {
      throw new ReplyNotFoundException();
    }

    const replies: Reply[] = await this.repliesRepository
      .createQueryBuilder('reply')
      .leftJoinAndSelect('reply.topicParent', 'topicParent')
      .where('topicParent.id = :topicId', { topicId })
      .orderBy('reply.createdAt', 'ASC')
      .getMany();

    const replyPosition: number = replies.findIndex((reply) => reply.id === replyId);

    const pageNumber: number = Math.ceil((replyPosition + 1) / pageSize);

    return pageNumber;
  }
}
