import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { AudioModule } from '../audio/audio.module';
import { LogsModule } from 'src/services/logger/logs.module';
import { NotificationsModule } from 'src/services/notifications/notifications.module';
import { PermissionsModule } from 'src/services/permissions/permissions.module';
import { TopicsService } from './topics.service';
import { TopicsController } from './topics.controller';
import { Topic } from './entity/topic.entity';
import { Reply } from './entity/reply.entity';
import { RepliesService } from './replies.service';

@Module({
  imports: [
    UsersModule,
    AudioModule,
    TypeOrmModule.forFeature([Topic, Reply]),
    LogsModule,
    NotificationsModule,
    PermissionsModule,
  ],
  controllers: [TopicsController],
  providers: [TopicsService, RepliesService],
  exports: [TopicsService, RepliesService],
})
export class TopicsModule {}
