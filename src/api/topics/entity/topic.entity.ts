import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, OneToMany } from 'typeorm';
import { User } from '../../users/entity/user.entity';
import { Reply } from './reply.entity';

@Entity()
export class Topic {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.topics)
  author: User;

  @Column()
  isPinned: boolean;

  @Column()
  title: string;

  @Column()
  content: string;

  @Column()
  createdAt: Date;

  @Column()
  lastUpdated: Date;

  @ManyToMany(() => User, (user) => user.followingTopics)
  followedBy: User[];

  @OneToMany(() => Reply, (reply) => reply.topicParent)
  replies: Reply[];
}
