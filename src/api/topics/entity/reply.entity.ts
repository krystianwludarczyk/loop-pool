import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from '../../users/entity/user.entity';
import { Topic } from './topic.entity';

@Entity()
export class Reply {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.replies)
  author: User;

  @Column()
  content: string;

  @Column()
  createdAt: Date;

  @ManyToOne(() => Topic, (topic) => topic.replies, { onDelete: 'CASCADE' })
  topicParent: Topic;
}
