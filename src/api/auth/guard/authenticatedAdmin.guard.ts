import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { InsufficientPermissionsException } from 'src/exceptions/permissions/insufficient-permissions.exception';
import { UserUnauthorizedException } from 'src/exceptions/users/user-unauthorized.exception';

@Injectable()
export class AuthenticatedAdminGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const response = context.switchToHttp().getResponse();

    if (!request || !request.session || !request.session.user || !request.isAuthenticated()) {
      response.redirect('/');
      throw new UserUnauthorizedException();
    }

    if (!request.session.user.isUserAdmin) {
      throw new InsufficientPermissionsException();
    }

    return true;
  }
}
