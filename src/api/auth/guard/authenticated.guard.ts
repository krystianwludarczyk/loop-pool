import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { UserBannedException } from 'src/exceptions/users/user-banned.exception';
import { UserUnauthorizedException } from 'src/exceptions/users/user-unauthorized.exception';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();

    if (!request || !request.session || !request.session.user || !request.isAuthenticated()) {
      throw new UserUnauthorizedException();
    }

    if (request.session.user.banReason) {
      throw new UserBannedException(request.session.user.banReason);
    }

    return true;
  }
}
