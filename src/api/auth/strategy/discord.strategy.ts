import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-discord';
import { AuthService } from '../auth.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DiscordStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    const configService = new ConfigService();
    const clientId = configService.getOrThrow('OAUTH2_CLIENT_ID');
    const clientSecret = configService.getOrThrow('OAUTH2_CLIENT_SECRET');
    const callbackURL = configService.getOrThrow('OAUTH2_CALLBACK_URL');
    const scope = configService.getOrThrow('OAUTH2_SCOPE').split(',');

    super({
      clientID: clientId,
      clientSecret: clientSecret,
      callbackURL: callbackURL,
      scope: scope,
    });
  }

  async validate(accessToken: string, refreshToken: string, profile: any, done: any): Promise<any> {
    const { id, username, email } = profile;
    const userData = await this.authService.validateUser(id, username, email);
    return done(null, userData);
  }
}
