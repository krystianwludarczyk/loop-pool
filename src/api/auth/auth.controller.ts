import { Controller, Get, Req, Res, UseGuards } from '@nestjs/common';
import { DiscordAuthGuard } from './guard/discord.auth.guard';
import { LogsService } from 'src/services/logger/logs.service';
import { AuthService } from './auth.service';

@Controller()
export class AuthController {
  constructor(
    private readonly logsService: LogsService,
    private readonly authService: AuthService,
  ) {}

  @UseGuards(DiscordAuthGuard)
  @Get('/auth/login')
  async loginUser() {}

  @UseGuards(DiscordAuthGuard)
  @Get('/auth/login/success')
  async onSuccessLogin(@Req() req, @Res() res) {
    req.session.user = req.user;
    await this.authService.updateLastUserLogin(req.session.user.id);
    await this.logsService.createLog(
      `USER[${req.session.user.id}] LOGGED IN`,
      '',
      `${JSON.stringify(req.session.user)}`,
    );

    res.redirect('/');
  }

  @Get('/auth/logout')
  async logoutUser(@Req() req) {
    req.session.destroy();
  }
}
