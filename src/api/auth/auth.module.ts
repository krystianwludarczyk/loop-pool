import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { DiscordStrategy } from './strategy/discord.strategy';
import { SessionSerializer } from './serializer/session.serializer';
import { LogsModule } from 'src/services/logger/logs.module';

@Module({
  imports: [UsersModule, PassportModule.register({ session: true }), LogsModule],
  controllers: [AuthController],
  providers: [AuthService, DiscordStrategy, SessionSerializer],
})
export class AuthModule {}
