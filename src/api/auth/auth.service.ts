import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from '../users/entity/user.entity';
import { formatUsername } from 'src/helpers/format.helper';

@Injectable()
export class AuthService {
  constructor(private readonly usersService: UsersService) {}

  async updateLastUserLogin(userId: number) {
    const userData = await this.usersService.getByIdOrFail(userId);
    userData.lastOnline = new Date();
    await this.usersService.update(userData);
  }

  async validateUser(discordId: any, username: string, email: string): Promise<User> {
    const user = await this.usersService.getByDiscordId(discordId);
    if (user) {
      return user;
    }

    const usersCount = await this.usersService.getUsersCount();
    const isFirstUser = usersCount == 0;
    const now = new Date();

    const createdUser = await this.usersService.create({
      verified: false,
      username: formatUsername(`username${now.getTime()}`),
      discordId: discordId,
      showDiscordProfile: false,
      email: email,
      isUserAdmin: isFirstUser,
      isUserModerator: false,
      muteReason: null,
      banReason: null,
      registredAt: new Date(),
      lastOnline: new Date(),
      showLastOnline: true,
      avatarUrl: null,
      aboutMe: null,
      instagramUrl: null,
      soundCloudUrl: null,
      youtubeUrl: null,
      facebookUrl: null,
      spotifyUrl: null,
      premiumPlanExpiration: new Date(0),
      followers: [],
      following: [],
      audio: [],
      likedAudio: [],
      allowedAudio: [],
      comments: [],
      topics: [],
      replies: [],
      followingTopics: [],
      transactions: [],
      offer: null,
    });

    return await this.usersService.getById(createdUser.id);
  }
}
