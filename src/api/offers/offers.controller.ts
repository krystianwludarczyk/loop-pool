import {
  Controller,
  Req,
  Body,
  Post,
  UseGuards,
  Delete,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthenticatedGuard } from '../auth/guard/authenticated.guard';
import { LogsService } from 'src/services/logger/logs.service';
import { UsersService } from '../users/users.service';
import { formatTextareaToString } from 'src/helpers/format.helper';
import { NotificationsService } from 'src/services/notifications/notifications.service';
import { PermissionsService } from 'src/services/permissions/permissions.service';
import { OffersService } from './offers.service';
import { CreateOfferDto } from './dto/create-offer.dto';
import { ActionType } from 'src/services/permissions/action.enum';
import { Offer } from './entity/offer.entity';

@Controller()
export class OffersController {
  constructor(
    private readonly offersService: OffersService,
    private readonly logsService: LogsService,
    private readonly notificationsService: NotificationsService,
    private readonly usersService: UsersService,
    private readonly permissionsService: PermissionsService,
  ) {}

  @UseGuards(AuthenticatedGuard)
  @Post('/offers')
  async createOffer(@Body() body: CreateOfferDto, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const asUserOffer = await this.usersService.getUserOffer(asUserData.id);
    await this.permissionsService.validateOfferAction(ActionType.Create, {} as Offer, {
      ...asUserData,
      offer: asUserOffer,
    });

    const createdOfferData = await this.offersService.create({
      author: asUserData,
      category: body.category,
      createdAt: new Date(),
      content: formatTextareaToString(body.content),
    });

    const userFollowers = await this.usersService.getUserFollowers(asUserData.id);
    for (const follower of userFollowers) {
      await this.notificationsService.createNotification(
        `/offers/${createdOfferData.id}`,
        `<b>${createdOfferData.author.username}</b> opublikował ogłoszenie typu <b>${createdOfferData.category}</b>`,
        follower.id,
      );
    }

    const afterOfferData = await this.offersService.getByIdOrFail(createdOfferData.id);
    await this.logsService.createLog(
      `USER[${asUserData.id}] CREATED OFFER[${createdOfferData.id}]`,
      ``,
      `${JSON.stringify(afterOfferData)}`,
    );

    return createdOfferData;
  }

  @UseGuards(AuthenticatedGuard)
  @Delete('/offers/:id')
  async deleteOffer(@Param('id', ParseIntPipe) offerId: number, @Req() req) {
    const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
    const offerData = await this.offersService.getByIdOrFail(offerId);
    const author = await this.offersService.getOfferAuthor(offerId);

    await this.permissionsService.validateOfferAction(
      ActionType.Delete,
      { ...offerData, author },
      asUserData,
    );

    await this.offersService.delete(offerId);

    await this.logsService.createLog(
      `USER[${asUserData.id}] DELETED OFFER[${offerData.id}]`,
      `${JSON.stringify(offerData)}`,
      ``,
    );
  }
}
