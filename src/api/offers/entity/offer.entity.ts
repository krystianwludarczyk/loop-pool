import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne } from 'typeorm';
import { User } from '../../users/entity/user.entity';
import { OfferCategory } from '../enum/offer-category.enum';

@Entity()
export class Offer {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => User, (user) => user.offer)
  author: User;

  @Column()
  category: OfferCategory;

  @Column()
  content: string;

  @Column()
  createdAt: Date;
}
