import { IsEnum, IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { OfferCategory } from '../enum/offer-category.enum';

export class CreateOfferDto {
  @IsEnum(OfferCategory, { message: 'Niepoprawna kategoria.' })
  category: OfferCategory;

  @IsNotEmpty({ message: 'Treść nie może być pusta.' })
  @IsString()
  @MaxLength(1000, { message: 'Treść nie może być dłuższa niż 1000 znaków.' })
  content: string;
}
