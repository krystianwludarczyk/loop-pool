import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { AudioModule } from '../audio/audio.module';
import { LogsModule } from 'src/services/logger/logs.module';
import { NotificationsModule } from 'src/services/notifications/notifications.module';
import { PermissionsModule } from 'src/services/permissions/permissions.module';
import { Offer } from './entity/offer.entity';
import { OffersController } from './offers.controller';
import { OffersService } from './offers.service';

@Module({
  imports: [
    UsersModule,
    AudioModule,
    TypeOrmModule.forFeature([Offer]),
    LogsModule,
    NotificationsModule,
    PermissionsModule,
  ],
  controllers: [OffersController],
  providers: [OffersService],
  exports: [OffersService],
})
export class OffersModule {}
