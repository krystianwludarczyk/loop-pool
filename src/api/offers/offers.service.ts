import { Injectable } from '@nestjs/common';
import { Offer } from './entity/offer.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entity/user.entity';
import { OfferNotFoundException } from '../../exceptions/offers/offer-not-found.exception';

@Injectable()
export class OffersService {
  constructor(
    @InjectRepository(Offer)
    private offersRepository: Repository<Offer>,
  ) {}

  async create(offerData: Omit<Offer, 'id'>): Promise<Offer> {
    return await this.offersRepository.save(offerData);
  }

  async getByIdOrFail(offerId: number): Promise<Offer> {
    const offer = await this.offersRepository.findOne({
      where: { id: offerId },
    });

    if (!offer) {
      throw new OfferNotFoundException();
    }

    return offer;
  }

  async getByQuery(
    page = 1,
    pageSize = 20,
    keywords?: string,
    loadAuthors = false,
  ): Promise<{ offersData: Offer[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Offer> = this.offersRepository.createQueryBuilder('offer');

    if (keywords) {
      queryBuilder = queryBuilder.andWhere('offer.title LIKE :keywords', {
        keywords: `%${keywords}%`,
      });
    }

    if (loadAuthors) {
      queryBuilder = queryBuilder.leftJoinAndSelect('offer.author', 'author');
    }

    const totalResults: number = await queryBuilder.getCount();
    const totalPages: number = Math.ceil(totalResults / pageSize);
    queryBuilder.skip((page - 1) * pageSize).take(pageSize);
    const offersData: Offer[] = await queryBuilder.getMany();

    return { offersData, totalPages };
  }

  async update(offerData: Offer): Promise<Offer> {
    return await this.offersRepository.save(offerData);
  }

  async delete(offerId: number): Promise<void> {
    const offerData = await this.getByIdOrFail(offerId);
    await this.offersRepository.remove(offerData);
  }

  /* RELATIONS */
  async getOfferAuthor(offerId: number): Promise<User> {
    const user: User = await this.offersRepository
      .createQueryBuilder('offer')
      .relation(Offer, 'author')
      .of(offerId)
      .loadOne();

    return user;
  }

  /* HELPERS */
  async getOfferPageNumber(offerId: number, pageSize = 20): Promise<number> {
    const offer = await this.offersRepository.findOne({ where: { id: offerId } });
    if (!offer) {
      throw new OfferNotFoundException();
    }

    const offers: Offer[] = await this.offersRepository.find({ order: { createdAt: 'DESC' } });
    const offerPosition: number = offers.findIndex((offer) => offer.id === offerId);
    const pageNumber: number = Math.ceil((offerPosition + 1) / pageSize);
    return pageNumber;
  }
}
