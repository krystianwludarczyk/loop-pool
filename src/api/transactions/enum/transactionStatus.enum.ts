export enum TransactionStatus {
  CREATED = 'CREATED',
  SUCCESS = 'SUCCESS',
  PENDING = 'PENDING',
  FAILURE = 'FAILURE',
}
