import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from '../../users/entity/user.entity';
import { TransactionStatus } from '../enum/transactionStatus.enum';

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  createdAt: Date;

  @Column()
  title: string;

  @Column()
  price: number;

  @Column({ nullable: true })
  paymentId: string;

  @Column()
  status: TransactionStatus;

  @Column({ default: false })
  benefitsDone: boolean;

  @ManyToOne(() => User, (user) => user.transactions)
  user: User;
}
