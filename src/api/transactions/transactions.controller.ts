import { Controller, Req, Body, Post, UseGuards, ForbiddenException } from '@nestjs/common';
import { AuthenticatedGuard } from '../auth/guard/authenticated.guard';
import { LogsService } from 'src/services/logger/logs.service';
import { UsersService } from '../users/users.service';
import { TransactionsService } from './transactions.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { ConfigService } from '@nestjs/config';
import { TransactionStatus } from './enum/transactionStatus.enum';
import { UpdateTransactionDto } from './dto/update-transaction.dto';

@Controller()
export class TransactionsController {
  constructor(
    private readonly transactionsService: TransactionsService,
    private readonly usersService: UsersService,
    private readonly logsService: LogsService,
    private readonly configService: ConfigService,
  ) {}

  @UseGuards(AuthenticatedGuard)
  @Post('/transactions')
  async createTransaction(@Body() body: CreateTransactionDto, @Req() req) {
    const user = await this.usersService.getByIdOrFail(req.session.user.id);
    const price = await this.configService.getOrThrow(`${body.premiumType}`);
    const secret = await this.configService.getOrThrow(`PAYMENT_SECRET`);

    const createdTransaction = await this.transactionsService.create({
      price,
      user,
      benefitsDone: false,
      status: TransactionStatus.CREATED,
      paymentId: null,
      title: body.premiumType,
      createdAt: new Date(),
    });

    await this.logsService.createLog(
      `USER[${user.id}] CREATED TRANSACTION[${createdTransaction.id}]`,
      ``,
      `${JSON.stringify(createdTransaction)}`,
    );

    return {
      paymentLink: `https://platnosc.hotpay.pl/?SEKRET=${secret}&KWOTA=${price}&NAZWA_USLUGI=Premium%20-%20${body.premiumType}&ADRES_WWW=https%3A%2F%2Floop-pool.pl%2Fpremium&ID_ZAMOWIENIA=${createdTransaction.id}`,
    };
  }

  @Post('/update-transaction')
  async updateTransaction(@Body() body: UpdateTransactionDto) {
    const secret = await this.configService.getOrThrow(`PAYMENT_SECRET`);
    if (secret !== body.SEKRET) {
      throw new ForbiddenException();
    }

    const transactionId = parseInt(body.ID_ZAMOWIENIA);
    const transactionData = await this.transactionsService.getByIdOrFail(transactionId, true);

    const beforeEditTransactionData = await this.transactionsService.getByIdOrFail(transactionId);
    await this.transactionsService.update({
      ...transactionData,
      status: body.STATUS,
      paymentId: body.ID_PLATNOSCI,
    });
    const afterEditTransactionData = await this.transactionsService.getByIdOrFail(transactionId);

    await this.logsService.createLog(
      `PAYMENTS_SOURCE UPDATED TRANSACTION[${transactionData.id}]`,
      `${JSON.stringify(beforeEditTransactionData)}`,
      `${JSON.stringify(afterEditTransactionData)}`,
    );

    if (body.STATUS === TransactionStatus.SUCCESS && transactionData.benefitsDone === false) {
      const user = await this.usersService.getByIdOrFail(transactionData.user.id);
      const beforeEditUserData = await this.usersService.getByIdOrFail(user.id);

      const days = parseInt(transactionData.title.slice(1));
      const expirationDate = this.usersService.isUserPremium(user)
        ? user.premiumPlanExpiration
        : new Date();
      expirationDate.setDate(expirationDate.getDate() + days);

      user.premiumPlanExpiration = expirationDate;
      await this.usersService.update(user);
      const afterEditUserData = await this.usersService.getByIdOrFail(user.id);

      const transactionDataSource = await this.transactionsService.getByIdOrFail(transactionId);
      await this.transactionsService.update({
        ...transactionDataSource,
        benefitsDone: true,
      });

      await this.logsService.createLog(
        `USER[${user.id}] PREMIUM UPDATED`,
        `${JSON.stringify(beforeEditUserData.premiumPlanExpiration)}`,
        `${JSON.stringify(afterEditUserData.premiumPlanExpiration)}`,
      );
    }
  }
}
