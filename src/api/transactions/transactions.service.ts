import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Transaction } from './entity/transaction.entity';
import { TransactionNotFoundException } from '../../exceptions/transactions/transaction-not-found.exception';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(Transaction)
    private transactionsRepository: Repository<Transaction>,
  ) {}

  async create(transactionData: Omit<Transaction, 'id'>): Promise<Transaction> {
    return await this.transactionsRepository.save(transactionData);
  }

  async getByIdOrFail(transactionId: number, loadRelations = false): Promise<Transaction> {
    const transaction = await this.transactionsRepository.findOne({
      where: { id: transactionId },
      relations: loadRelations ? ['user'] : null,
    });

    if (!transaction) {
      throw new TransactionNotFoundException();
    }

    return transaction;
  }

  async update(transactionData: Transaction): Promise<Transaction> {
    return await this.transactionsRepository.save(transactionData);
  }

  async getByQuery(
    page = 1,
    pageSize = 20,
    loadRelations = false,
  ): Promise<{ transactionsData: Transaction[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Transaction> =
      this.transactionsRepository.createQueryBuilder('transaction');

    if (loadRelations) {
      queryBuilder = queryBuilder.leftJoinAndSelect('transaction.user', 'user');
    }

    queryBuilder = queryBuilder.orderBy('transaction.createdAt', 'DESC');

    const totalResults: number = await queryBuilder.getCount();
    const totalPages: number = Math.ceil(totalResults / pageSize);
    queryBuilder.skip((page - 1) * pageSize).take(pageSize);
    const transactionsData: Transaction[] = await queryBuilder.getMany();

    return { transactionsData, totalPages };
  }
}
