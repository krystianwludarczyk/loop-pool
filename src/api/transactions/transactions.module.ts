import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { AudioModule } from '../audio/audio.module';
import { LogsModule } from 'src/services/logger/logs.module';
import { NotificationsModule } from 'src/services/notifications/notifications.module';
import { PermissionsModule } from 'src/services/permissions/permissions.module';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { Transaction } from './entity/transaction.entity';

@Module({
  imports: [
    UsersModule,
    AudioModule,
    TypeOrmModule.forFeature([Transaction]),
    LogsModule,
    NotificationsModule,
    PermissionsModule,
  ],
  controllers: [TransactionsController],
  providers: [TransactionsService],
  exports: [TransactionsService],
})
export class TransactionsModule {}
