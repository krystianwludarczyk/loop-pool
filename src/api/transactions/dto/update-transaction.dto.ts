import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { TransactionStatus } from '../enum/transactionStatus.enum';

export class UpdateTransactionDto {
  @IsNotEmpty()
  @IsString()
  SEKRET: string;

  @IsNotEmpty()
  @IsEnum(TransactionStatus)
  STATUS: TransactionStatus;

  @IsNotEmpty()
  @IsString()
  ID_ZAMOWIENIA: string;

  @IsNotEmpty()
  @IsString()
  ID_PLATNOSCI: string;
}
