import { IsEnum, IsNotEmpty } from 'class-validator';
import { PremiumType } from '../enum/premiumType.enum';

export class CreateTransactionDto {
  @IsNotEmpty({ message: 'Plan członkostwa nie może być pusty.' })
  @IsEnum(PremiumType, { message: 'Niepoprawny plan członkostwa.' })
  premiumType: PremiumType;
}
