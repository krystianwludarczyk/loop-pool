import { HttpException, HttpStatus } from '@nestjs/common';

export class TransactionNotFoundException extends HttpException {
  constructor() {
    super('Transakcja nie istnieje.', HttpStatus.NOT_FOUND);
  }
}
