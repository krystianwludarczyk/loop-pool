import { HttpException, HttpStatus } from '@nestjs/common';

export class FileWrongMimetypeException extends HttpException {
  constructor() {
    super('Wymagany format pliku to WAV lub MP3.', HttpStatus.BAD_REQUEST);
  }
}
