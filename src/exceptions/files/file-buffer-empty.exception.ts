import { HttpException, HttpStatus } from '@nestjs/common';

export class FileBufferEmptyException extends HttpException {
  constructor() {
    super('Bufor pliku jest pusty.', HttpStatus.BAD_REQUEST);
  }
}

