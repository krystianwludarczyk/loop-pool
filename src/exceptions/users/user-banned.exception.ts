import { HttpException, HttpStatus } from '@nestjs/common';

export class UserBannedException extends HttpException {
  constructor(reason: string) {
    super(`Zostałeś zbanowany. Powód: ${reason}`, HttpStatus.FORBIDDEN);
  }
}
