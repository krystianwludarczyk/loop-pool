import { HttpException, HttpStatus } from '@nestjs/common';

export class UserNameExistsException extends HttpException {
  constructor() {
    super('Użytkownik o tej nazwie użytkownika już istnieje.', HttpStatus.BAD_REQUEST);
  }
}
