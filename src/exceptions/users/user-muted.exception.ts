import { HttpException, HttpStatus } from '@nestjs/common';

export class UserMutedException extends HttpException {
  constructor(reason: string) {
    super(`Zostałeś wyciszony. Powód: ${reason}`, HttpStatus.FORBIDDEN);
  }
}
