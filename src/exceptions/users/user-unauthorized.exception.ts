import { HttpException, HttpStatus } from '@nestjs/common';

export class UserUnauthorizedException extends HttpException {
  constructor() {
    super('Zaloguj się, aby zobaczyć tę zawartość.', HttpStatus.UNAUTHORIZED);
  }
}
