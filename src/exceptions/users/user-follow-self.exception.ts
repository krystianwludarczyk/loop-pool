import { HttpException, HttpStatus } from '@nestjs/common';

export class UserFollowSelfException extends HttpException {
  constructor() {
    super('Nie możesz obserwować siebie.', HttpStatus.NOT_FOUND);
  }
}
