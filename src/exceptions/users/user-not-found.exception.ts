import { HttpException, HttpStatus } from '@nestjs/common';

export class UserNotFoundException extends HttpException {
  constructor() {
    super('Użytkownik nie istnieje.', HttpStatus.NOT_FOUND);
  }
}

