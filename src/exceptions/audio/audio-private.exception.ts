import { HttpException, HttpStatus } from '@nestjs/common';

export class AudioPrivateException extends HttpException {
  constructor() {
    super('Plik jest prywatny.', HttpStatus.FORBIDDEN);
  }
}
