import { HttpException, HttpStatus } from '@nestjs/common';

export class AudioNotFoundException extends HttpException {
  constructor() {
    super('Plik nie istnieje.', HttpStatus.NOT_FOUND);
  }
}
