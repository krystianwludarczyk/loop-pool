import { HttpException, HttpStatus } from '@nestjs/common';

export class AudioNotPrivateException extends HttpException {
  constructor() {
    super('Plik nie jest prywatny.', HttpStatus.BAD_REQUEST);
  }
}
