import { HttpException, HttpStatus } from '@nestjs/common';

export class OfferExistsException extends HttpException {
  constructor() {
    super('Już posiadasz aktywną ofertę.', HttpStatus.FORBIDDEN);
  }
}
