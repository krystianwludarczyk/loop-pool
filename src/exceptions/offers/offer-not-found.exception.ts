import { HttpException, HttpStatus } from '@nestjs/common';

export class OfferNotFoundException extends HttpException {
  constructor() {
    super('Oferta nie istnieje.', HttpStatus.NOT_FOUND);
  }
}
