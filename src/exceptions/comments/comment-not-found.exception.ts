import { HttpException, HttpStatus } from '@nestjs/common';

export class CommentNotFoundException extends HttpException {
  constructor() {
    super('Komentarz nie istnieje.', HttpStatus.NOT_FOUND);
  }
}
