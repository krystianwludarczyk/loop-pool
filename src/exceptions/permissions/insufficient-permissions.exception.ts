import { HttpException, HttpStatus } from '@nestjs/common';

export class InsufficientPermissionsException extends HttpException {
  constructor() {
    super('Brak wymaganych uprawnień.', HttpStatus.FORBIDDEN);
  }
}
