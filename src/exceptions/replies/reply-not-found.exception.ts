import { HttpException, HttpStatus } from '@nestjs/common';

export class ReplyNotFoundException extends HttpException {
  constructor() {
    super('Odpowiedź nie istnieje.', HttpStatus.NOT_FOUND);
  }
}
