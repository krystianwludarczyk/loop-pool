import { HttpException, HttpStatus } from '@nestjs/common';

export class TopicNotFoundException extends HttpException {
  constructor() {
    super('Temat nie istnieje.', HttpStatus.NOT_FOUND);
  }
}
