import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Query,
  Req,
  Res,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AppService } from './app.service';
import { AuthenticatedGuard } from './api/auth/guard/authenticated.guard';
import { AuthenticatedStaffGuard } from './api/auth/guard/authenticatedStaff.guard';
import { AuthenticatedAdminGuard } from './api/auth/guard/authenticatedAdmin.guard';
import { UsersService } from './api/users/users.service';
import { NotificationsService } from './services/notifications/notifications.service';
import {
  formatDateToDateString,
  formatDateToDetailedDateString,
  formatDateToInputDate,
  formatStringToTextarea,
} from './helpers/format.helper';
import { AudioService } from './api/audio/audio.service';
import { CommentsService } from './api/audio/comments.service';
import { ActionType } from './services/permissions/action.enum';
import { PermissionsService } from './services/permissions/permissions.service';
import { TopicsService } from './api/topics/topics.service';
import { RepliesService } from './api/topics/replies.service';
import { AudioNotPrivateException } from './exceptions/audio/audio-not-private.exception';
import { Audio } from './api/audio/entity/audio.entity';
import { OffersService } from './api/offers/offers.service';
import { UserUnauthorizedException } from './exceptions/users/user-unauthorized.exception';
import { HttpExceptionFilter } from './exceptions/HttpExceptionFilter';

@UseFilters(HttpExceptionFilter)
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly usersService: UsersService,
    private readonly audioService: AudioService,
    private readonly commentsService: CommentsService,
    private readonly notificationsService: NotificationsService,
    private readonly permissionsService: PermissionsService,
    private readonly topicsService: TopicsService,
    private readonly repliesService: RepliesService,
    private readonly offersService: OffersService,
  ) {}

  @Get('/')
  async getHome(
    @Query('keywords') keywords: string,
    @Query('page') page: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      let localUserData;
      if (req.session && req.session.user) {
        const asUserData = await this.usersService.getById(req.session.user.id);
        const newNotifications =
          req.session == true
            ? await this.notificationsService.getUserNewNotificationsCount(asUserData.id)
            : null;
        const isAsUserPremium = this.usersService.isUserPremium(asUserData);
        localUserData = {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        };
      } else {
        localUserData = false;
      }

      const queryResponse = await this.topicsService.getByQuery(page, 20, keywords, true);
      const topicsData = await Promise.all(
        queryResponse.topicsData.map(async (topic) => ({
          ...topic,
          createdAt: formatDateToDateString(topic.createdAt, false),
          author: {
            ...topic.author,
            isUserPremium: this.usersService.isUserPremium(topic.author),
            isUserOnline: this.appService.isUserOnline(topic.author),
          },
        })),
      );

      res.render('home', {
        topicsData,
        totalPages: queryResponse.totalPages,
        localUserData,
      });
    } catch (error) {
      console.error(error);
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/notifications')
  async getNotificationsPage(@Res() res, @Req() req) {
    try {
      const data = await this.appService.getNotificationsPage(req.session.user.id);
      res.render('notifications', data);
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  /******************************************************************** AUDIO */
  /******************************************************************** AUDIO */
  /******************************************************************** AUDIO */
  @UseGuards(AuthenticatedGuard)
  @Get('/create-audio')
  async createAudioPage(@Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      await this.permissionsService.validateAudioAction(ActionType.Create, {} as Audio, asUserData);

      res.render('audio/create-audio', {
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: this.usersService.isUserPremium(asUserData),
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @Get('/audio')
  async searchAudio(
    @Query() query: Record<string, any>,
    @Query('page') page: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      let localUserData;
      if (req.session && req.session.user) {
        const asUserData = await this.usersService.getById(req.session.user.id);
        const newNotifications =
          req.session == true
            ? await this.notificationsService.getUserNewNotificationsCount(asUserData.id)
            : null;
        const isAsUserPremium = this.usersService.isUserPremium(asUserData);
        localUserData = {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        };
      } else {
        localUserData = false;
      }

      const audio = await this.audioService.getByQuery(query, false, page);
      const audioData = await Promise.all(
        audio.results.map(async (audio) => ({
          ...audio,
          author: await this.audioService.getAudioAuthor(audio.id),
          createdAt: formatDateToDateString(audio.createdAt, false),
        })),
      );

      res.render('audio/search-audio', {
        audioData,
        totalPages: audio.totalPages,
        localUserData,
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/audio/:id')
  async getAudioPage(
    @Param('id', ParseIntPipe) audioId: number,
    @Query('page') page: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const asUserData = await this.usersService.getById(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const audioData = await this.audioService.getByIdOrFail(audioId);
      const authorizedUsers = await this.audioService.getAudioAuthorizedUsers(audioId);
      const audioAuthor = await this.audioService.getAudioAuthor(audioId);

      await this.permissionsService.validateAudioAction(
        ActionType.Read,
        { ...audioData, author: audioAuthor, authorizedUsers },
        asUserData,
      );

      const commentsRawData = await this.commentsService.getCommentsByAudio(
        audioId,
        page,
        20,
        true,
      );
      const commentsData = commentsRawData.commentsData.map((comment) => ({
        ...comment,
        createdAt: formatDateToDetailedDateString(comment.createdAt),
        isUserAuthor: comment.author.id === asUserData.id,
        author: {
          ...comment.author,
          isUserPremium: this.usersService.isUserPremium(comment.author),
          isUserOnline: this.appService.isUserOnline(comment.author),
        },
      }));

      const isAudioLiked = await this.audioService.isLikedByUser(audioData.id, asUserData.id);
      const audioLikesCount = await this.audioService.getAudioLikesCount(audioData.id);
      const isAudioOwner = asUserData.id === audioAuthor.id;

      res.render('audio/audio', {
        audioData: {
          ...audioData,
          author: {
            ...audioAuthor,
            isUserPremium: this.usersService.isUserPremium(audioAuthor),
            isUserOnline: this.appService.isUserOnline(audioAuthor),
          },

          likes: audioLikesCount,
          comments: commentsData,
          createdAt: formatDateToDetailedDateString(audioData.createdAt),
        },
        totalPages: commentsRawData.totalPages,
        localUserData: {
          ...asUserData,
          isAudioLiked,
          isAudioOwner,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/audio/:id/edit')
  async getEditAudioPage(@Param('id', ParseIntPipe) audioId: number, @Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const audioData = await this.audioService.getByIdOrFail(audioId);
      const audioAuthor = await this.audioService.getAudioAuthor(audioId);

      await this.permissionsService.validateAudioAction(
        ActionType.Edit,
        { ...audioData, author: audioAuthor },
        asUserData,
      );

      res.render('audio/edit-audio', {
        audioData: {
          ...audioData,
          description: formatStringToTextarea(audioData.description),
        },
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/audio/:id/edit/access-control')
  async getAccessControlAudioPage(
    @Param('id', ParseIntPipe) audioId: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);
      const authorizedUsers = await this.audioService.getAudioAuthorizedUsers(audioId);

      const audioData = await this.audioService.getByIdOrFail(audioId);
      const audioAuthor = await this.audioService.getAudioAuthor(audioId);

      await this.permissionsService.validateAudioAction(
        ActionType.Edit,
        { ...audioData, author: audioAuthor },
        asUserData,
      );

      if (!audioData.isPrivate) {
        throw new AudioNotPrivateException();
      }

      res.render('audio/access-control-audio', {
        audioData: {
          ...audioData,
          authorizedUsers,
        },
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @Get('/comments/:id')
  async getComment(@Param('id', ParseIntPipe) commentId: number, @Res() res) {
    const replyData = await this.commentsService.getByIdOrFail(commentId, true);
    const page = await this.commentsService.getCommentPageNumber(
      replyData.audioParent.id,
      commentId,
    );
    res.redirect(`/audio/${replyData.audioParent.id}?page=${page}#comment-${replyData.id}`);
  }

  /******************************************************************* TOPICS */
  /******************************************************************* TOPICS */
  /******************************************************************* TOPICS */

  @UseGuards(AuthenticatedGuard)
  @Get('/topics/:id')
  async getTopicPage(
    @Param('id', ParseIntPipe) topicId: number,
    @Query('page') page: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const topicData = await this.topicsService.getByIdOrFail(topicId);
      const topicAuthor = await this.topicsService.getTopicAuthor(topicId);
      const isTopicFollowed = await this.topicsService.isTopicFollowedByUser(
        topicId,
        asUserData.id,
      );
      const isTopicAuthor = asUserData.id === topicAuthor.id;

      const repliesRawData = await this.repliesService.getRepliesByTopic(topicId, page, 20, true);
      const repliesData = await Promise.all(
        repliesRawData.repliesData.map(async (reply) => ({
          ...reply,
          createdAt: formatDateToDateString(reply.createdAt, true),
          isUserAuthor: reply.author.id === asUserData.id,
          author: {
            ...reply.author,
            isUserPremium: this.usersService.isUserPremium(reply.author),
            isUserOnline: await this.appService.isUserOnline(reply.author),
          },
        })),
      );

      res.render('topics/topic', {
        topicData: {
          ...topicData,
          contentRaw: formatStringToTextarea(topicData.content),
          createdAt: formatDateToDateString(topicData.createdAt, true),
          replies: repliesData,
          author: {
            ...topicAuthor,
            isUserPremium: this.usersService.isUserPremium(topicAuthor),
            isUserOnline: this.appService.isUserOnline(topicAuthor),
          },
        },
        totalPages: repliesRawData.totalPages,
        localUserData: {
          ...asUserData,
          newNotifications,
          isTopicFollowed,
          isTopicAuthor,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @Get('/replies/:id')
  async getReply(@Param('id', ParseIntPipe) replyId: number, @Res() res) {
    const replyData = await this.repliesService.getByIdOrFail(replyId, true);
    const page = await this.repliesService.getReplyPageNumber(replyData.topicParent.id, replyId);
    res.redirect(`/topics/${replyData.topicParent.id}?page=${page}#reply-${replyData.id}`);
  }

  /************************************************************* TRANSACTIONS */
  /************************************************************* TRANSACTIONS */
  /************************************************************* TRANSACTIONS */

  @UseGuards(AuthenticatedGuard)
  @Get('/premium')
  async getPremiumPage(@Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );

      res.render('premium', {
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: this.usersService.isUserPremium(asUserData),
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  /******************************************************************** USERS */
  /******************************************************************** USERS */
  /******************************************************************** USERS */
  @Get('/users')
  async searchUsers(
    @Query() query: Record<string, any>,
    @Query('page') page: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      let localUserData;
      if (req.session && req.session.user) {
        const asUserData = await this.usersService.getById(req.session.user.id);
        const newNotifications =
          req.session == true
            ? await this.notificationsService.getUserNewNotificationsCount(asUserData.id)
            : null;
        const isAsUserPremium = this.usersService.isUserPremium(asUserData);
        localUserData = {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        };
      } else {
        localUserData = false;
      }

      const queryResponse = await this.usersService.getByQuery(query, page);
      const usersData = await Promise.all(
        queryResponse.usersData.map(async (user) => ({
          ...user,
          isUserPremium: this.usersService.isUserPremium(user),
          isUserOnline: await this.appService.isUserOnline(user),
        })),
      );

      res.render('users/users', {
        usersData,
        totalPages: queryResponse.totalPages,
        localUserData,
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/users/:id')
  async getUserPage(
    @Query('page') page: number,
    @Param('id', ParseIntPipe) id: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );

      const userData = await this.usersService.getByIdOrFail(id);
      const isProfileOwner = userData.id === asUserData.id;
      const result = await this.audioService.getAudioByUser(id, isProfileOwner, page);
      const formatedAudioData = result.audioData.map((audio) => ({
        ...audio,
        createdAt: formatDateToDateString(audio.createdAt, false),
      }));

      const userFollowers = await this.usersService.getUserFollowersCount(id);
      const userFollowing = await this.usersService.getUserFollowingCount(id);

      res.render('users/user', {
        totalPages: result.totalPages,
        userData: {
          ...userData,
          registredAt: formatDateToDateString(userData.registredAt, false),
          audio: formatedAudioData,
          followers: userFollowers,
          following: userFollowing,
          isUserPremium: this.usersService.isUserPremium(userData),
          lastOnline: formatDateToDateString(userData.lastOnline, false),
          isUserOnline: this.appService.isUserOnline(userData),
        },
        localUserData: {
          ...asUserData,
          isUserFollower: await this.usersService.isUserFollower(userData.id, asUserData.id),
          isProfileOwner,
          newNotifications,
          isUserPremium: this.usersService.isUserPremium(asUserData),
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/users/:id/followers')
  async getUserFollowersPage(@Param('id', ParseIntPipe) id: number, @Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const userData = await this.usersService.getByIdOrFail(id);
      const userFollowers = await this.usersService.getUserFollowers(id);

      res.render('users/user-followers', {
        userData: { ...userData, followers: userFollowers },
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/users/:id/following')
  async getUserFollowingPage(@Param('id', ParseIntPipe) id: number, @Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const userData = await this.usersService.getByIdOrFail(id);
      const userFollowing = await this.usersService.getUserFollowing(id);

      res.render('users/user-following', {
        userData: { ...userData, following: userFollowing },
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/users/:id/edit')
  async getEditUserPage(@Param('id', ParseIntPipe) userId: number, @Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const userData = await this.usersService.getByIdOrFail(userId);
      await this.permissionsService.validateUserAction(ActionType.Edit, userData, asUserData);

      res.render('users/edit-user', {
        userData: {
          ...userData,
          aboutMe: formatStringToTextarea(userData.aboutMe),
        },
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/users/:id/manage')
  async getManageUserPage(@Param('id', ParseIntPipe) userId: number, @Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const userData = await this.usersService.getByIdOrFail(userId);
      await this.permissionsService.validateUserAction(ActionType.Manage, userData, asUserData);

      res.render('users/manage-user', {
        userData: {
          ...userData,
          aboutMe: formatStringToTextarea(userData.aboutMe),
          premiumPlanExpiration: formatDateToInputDate(userData.premiumPlanExpiration),
        },
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  /* OFFERS */
  @UseGuards(AuthenticatedGuard)
  @Get('/offers')
  async searchOffers(
    @Query('page') page: number,
    @Query('keywords') keywords: string,
    @Req() req,
    @Res() res,
  ) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      const offers = await this.offersService.getByQuery(page, 20, keywords, true);
      const offersData = await Promise.all(
        offers.offersData.map(async (offer) => ({
          ...offer,
          createdAt: formatDateToDateString(offer.createdAt, false),
          isUserAuthor: offer.author.id === asUserData.id,
        })),
      );

      res.render('offers/offers', {
        offersData,
        totalPages: offers.totalPages,
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @Get('/offers/:id')
  async getOffer(@Param('id', ParseIntPipe) offerId: number, @Res() res) {
    const offerData = await this.offersService.getByIdOrFail(offerId);
    const page = await this.offersService.getOfferPageNumber(offerId);
    res.redirect(`/offers?page=${page}#offer-${offerData.id}`);
  }

  /******************************************************************** ADMIN */
  /******************************************************************** ADMIN */
  /******************************************************************** ADMIN */

  @UseGuards(AuthenticatedStaffGuard)
  @Get('/admin/dashboard')
  async getAdminDashboardPage(@Req() req, @Res() res) {
    try {
      const asUserData = await this.usersService.getByIdOrFail(req.session.user.id);
      const newNotifications = await this.notificationsService.getUserNewNotificationsCount(
        asUserData.id,
      );
      const isAsUserPremium = this.usersService.isUserPremium(asUserData);

      res.render('admin/dashboard', {
        localUserData: {
          ...asUserData,
          newNotifications,
          isUserPremium: isAsUserPremium,
        },
      });
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedStaffGuard)
  @Get('/admin/dashboard/logs')
  async getAdminLogsPage(
    @Query() query: Record<string, any>,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const data = await this.appService.searchLogs(query, page, pageSize, req.session.user.id);
      res.render('admin/logs', data);
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedStaffGuard)
  @Get('/admin/dashboard/comments')
  async getAdminCommentsPage(
    @Query() query: Record<string, any>,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const data = await this.appService.searchComments(query, page, pageSize, req.session.user.id);
      res.render('admin/comments', data);
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedStaffGuard)
  @Get('/admin/dashboard/replies')
  async getAdminRepliesPage(
    @Query() query: Record<string, any>,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const data = await this.appService.searchReplies(query, page, pageSize, req.session.user.id);
      res.render('admin/replies', data);
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }

  @UseGuards(AuthenticatedAdminGuard)
  @Get('/admin/dashboard/transactions')
  async getAdminTransactionsPage(
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Req() req,
    @Res() res,
  ) {
    try {
      const data = await this.appService.searchTransactions(page, pageSize, req.session.user.id);
      res.render('admin/transactions', data);
    } catch (error) {
      res.render('error', {
        error,
        backUrl: req.header('Referer') || '/',
      });
    }
  }
}
