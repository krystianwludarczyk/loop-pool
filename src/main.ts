import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import * as session from 'express-session';
import * as passport from 'passport';
import * as hbs from 'hbs';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { HttpExceptionFilter } from './exceptions/HttpExceptionFilter';

async function bootstrap() {
  const configService = new ConfigService();

  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.setBaseViewsDir(join(__dirname, '..', '..', 'views'));
  hbs.registerPartials(join(__dirname, '..', '..', 'views', 'partials'));
  app.useStaticAssets(join(__dirname, '..', '..', 'public'));
  app.setViewEngine('hbs');

  app.use(
    session({
      secret: configService.getOrThrow('SESSION_SECRET'),
      resave: false,
      saveUninitialized: false,
      rolling: false,
      cookie: {
        maxAge: 1 * 3600000,
        secure: false,
      },
    }),
  );

  app.use(passport.initialize());
  app.use(passport.session());
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(configService.getOrThrow('PORT'));
}
bootstrap();
