import { Injectable } from '@nestjs/common';
import { formatDateToDateString } from './helpers/format.helper';
import { UsersService } from './api/users/users.service';
import { LogsService } from './services/logger/logs.service';
import { CommentsService } from './api/audio/comments.service';
import { NotificationsService } from './services/notifications/notifications.service';
import { RepliesService } from './api/topics/replies.service';
import { TransactionsService } from './api/transactions/transactions.service';
import { User } from './api/users/entity/user.entity';

@Injectable()
export class AppService {
  constructor(
    private readonly usersService: UsersService,
    private readonly commentsService: CommentsService,
    private readonly repliesService: RepliesService,
    private readonly transactionsService: TransactionsService,
    private readonly logsService: LogsService,
    private readonly notificationsService: NotificationsService,
  ) {}

  isUserOnline(userData: User): boolean {
    if (userData.showLastOnline === false) {
      return false;
    }

    const fifteenMinutesAgo = new Date();
    fifteenMinutesAgo.setMinutes(fifteenMinutesAgo.getMinutes() - 15);
    return userData.lastOnline > fifteenMinutesAgo;
  }

  //PAGES
  async getNotificationsPage(asUserId: number): Promise<any> {
    const asUserData = await this.usersService.getByIdOrFail(asUserId);

    const notifications = await this.notificationsService.getUserNotifications(asUserId);
    const notificationsData = notifications.map((notification) => ({
      ...notification,
      createdAt: formatDateToDateString(notification.createdAt, true),
    }));
    await this.notificationsService.resetUserNotifications(asUserId);
    const newNotifications = await this.notificationsService.getUserNewNotificationsCount(asUserId);

    return {
      notificationsData,
      localUserData: {
        ...asUserData,
        newNotifications,
        isUserPremium: this.usersService.isUserPremium(asUserData),
      },
    };
  }

  async searchLogs(
    query: Record<string, any>,
    page: number,
    pageSize: number,
    asUserId: number,
  ): Promise<any> {
    const asUserData = await this.usersService.getByIdOrFail(asUserId);
    const newNotifications = await this.notificationsService.getUserNewNotificationsCount(asUserId);

    const result = await this.logsService.getByQuery(query, page, pageSize);
    const logsData = result.logsData.map((log) => ({
      ...log,
      createdAt: formatDateToDateString(log.createdAt, true),
      rawCreatedAt: log.createdAt.getTime(),
    }));

    return {
      logsData,
      totalPages: result.totalPages,
      localUserData: {
        ...asUserData,
        newNotifications,
        isUserPremium: this.usersService.isUserPremium(asUserData),
      },
    };
  }

  async searchComments(
    query: Record<string, any>,
    page: number,
    pageSize: number,
    asUserId: number,
  ): Promise<any> {
    const asUserData = await this.usersService.getByIdOrFail(asUserId);
    const newNotifications = await this.notificationsService.getUserNewNotificationsCount(asUserId);

    const result = await this.commentsService.getByQuery(query, page, pageSize, true);
    const commentsData = await Promise.all(
      result.commentsData.map(async (comment) => ({
        ...comment,
        createdAt: formatDateToDateString(comment.createdAt, true),
        page: await this.commentsService.getCommentPageNumber(comment.audioParent.id, comment.id),
        author: {
          ...comment.author,
          isUserPremium: this.usersService.isUserPremium(comment.author),
        },
      })),
    );

    return {
      commentsData,
      totalPages: result.totalPages,
      localUserData: {
        ...asUserData,
        newNotifications,
        isUserPremium: this.usersService.isUserPremium(asUserData),
      },
    };
  }

  async searchReplies(
    query: Record<string, any>,
    page: number,
    pageSize: number,
    asUserId: number,
  ): Promise<any> {
    const asUserData = await this.usersService.getByIdOrFail(asUserId);
    const newNotifications = await this.notificationsService.getUserNewNotificationsCount(asUserId);

    const result = await this.repliesService.getByQuery(query, page, pageSize, true);
    const repliesData = await Promise.all(
      result.repliesData.map(async (reply) => ({
        ...reply,
        createdAt: formatDateToDateString(reply.createdAt, true),
        page: await this.repliesService.getReplyPageNumber(reply.topicParent.id, reply.id),
        author: {
          ...reply.author,
          isUserPremium: this.usersService.isUserPremium(reply.author),
        },
      })),
    );

    return {
      repliesData,
      totalPages: result.totalPages,
      localUserData: {
        ...asUserData,
        newNotifications,
        isUserPremium: this.usersService.isUserPremium(asUserData),
      },
    };
  }

  async searchTransactions(page: number, pageSize: number, asUserId: number): Promise<any> {
    const asUserData = await this.usersService.getByIdOrFail(asUserId);
    const newNotifications = await this.notificationsService.getUserNewNotificationsCount(asUserId);

    const result = await this.transactionsService.getByQuery(page, pageSize, true);
    const transactionsData = await Promise.all(
      result.transactionsData.map(async (transaction) => ({
        ...transaction,
        createdAt: formatDateToDateString(transaction.createdAt, true),
        rawCreatedAt: transaction.createdAt.getTime(),
        author: {
          ...transaction.user,
          isUserPremium: this.usersService.isUserPremium(transaction.user),
        },
      })),
    );

    return {
      transactionsData,
      totalPages: result.totalPages,
      localUserData: {
        ...asUserData,
        newNotifications,
        isUserPremium: this.usersService.isUserPremium(asUserData),
      },
    };
  }
}
