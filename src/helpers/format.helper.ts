export function formatTextareaToString(text: string): string {
  if (!text) {
    return text;
  }
  const formattedText = text
    .replace(/^\n+/, '')
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;')
    .replace(/\n\s*\n/g, '\n\n')
    .replace(/\n/g, '<br>');

  return formattedText;
}

export function formatStringToTextarea(text: string): string {
  if (!text) {
    return text;
  }
  const formattedText = text.replace(/<br>/g, '\n');

  return formattedText;
}

export function formatDateToInputDate(date: Date): string {
  return date.toISOString().split('T')[0];
}

export function formatDateToDetailedDateString(date: Date): string {
  const currentDate = new Date();
  const timeDifference = currentDate.getTime() - date.getTime();

  const secondsInMilli = 1000;
  const minutesInMilli = secondsInMilli * 60;
  const hoursInMilli = minutesInMilli * 60;
  const daysInMilli = hoursInMilli * 24;

  if (timeDifference < minutesInMilli) {
    const secondsAgo = Math.floor(timeDifference / secondsInMilli);
    return `${secondsAgo} sek temu`;
  } else if (timeDifference < hoursInMilli) {
    const minutesAgo = Math.floor(timeDifference / minutesInMilli);
    return `${minutesAgo} min temu`;
  } else if (timeDifference < daysInMilli) {
    const hoursAgo = Math.floor(timeDifference / hoursInMilli);
    return `${hoursAgo} godz temu`;
  } else if (timeDifference < daysInMilli * 7) {
    const daysAgo = Math.floor(timeDifference / daysInMilli);
    const pluralSuffix = daysAgo === 1 ? 'dzień' : 'dni';
    return `${daysAgo} ${pluralSuffix} temu`;
  } else {
    return date.toLocaleDateString('pl-PL', {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
    });
  }
}

export function formatDateToDateString(date: Date, showDetailed: boolean): string {
  const currentDate = new Date();
  const timeDifference = currentDate.getTime() - date.getTime();

  const secondsInMilli = 1000;
  const minutesInMilli = secondsInMilli * 60;
  const hoursInMilli = minutesInMilli * 60;
  const daysInMilli = hoursInMilli * 24;

  if (timeDifference < minutesInMilli) {
    const secondsAgo = Math.floor(timeDifference / secondsInMilli);
    return `${secondsAgo} sek temu`;
  } else if (timeDifference < hoursInMilli) {
    const minutesAgo = Math.floor(timeDifference / minutesInMilli);
    return `${minutesAgo} min temu`;
  } else if (timeDifference < daysInMilli) {
    const hoursAgo = Math.floor(timeDifference / hoursInMilli);
    return `${hoursAgo} godz temu`;
  } else if (timeDifference < daysInMilli * 7) {
    const daysAgo = Math.floor(timeDifference / daysInMilli);
    const pluralSuffix = daysAgo === 1 ? 'dzień' : 'dni';
    return `${daysAgo} ${pluralSuffix} temu`;
  } else {
    return date.toLocaleDateString(
      'pl-PL',
      showDetailed
        ? {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
          }
        : {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
          },
    );
  }
}

export function formatUsername(username: string): string {
  return username.replace(/[^a-zA-Z0-9]/g, '');
}
