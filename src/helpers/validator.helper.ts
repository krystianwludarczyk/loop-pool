import { BadRequestException } from '@nestjs/common';

export function validateImageUrl(imageUrl: string, asUserIsPremium: boolean): string {
  if (imageUrl.length == 0) {
    return imageUrl;
  }

  const supportedImageExtensions = ['.png', '.jpeg', '.jpg'];
  const supportedPremiumImageExtensions = ['.gif'];
  const lowercasedUrl = imageUrl.toLowerCase();

  const isImage = supportedImageExtensions.some((ext) => lowercasedUrl.endsWith(ext));
  const isPremiumImage = supportedPremiumImageExtensions.some((ext) => lowercasedUrl.endsWith(ext));

  if (!isImage && !isPremiumImage) {
    throw new BadRequestException(
      'Nieprawidłowy format obrazu. Obsługiwane formaty to: PNG, JPEG, JPG oraz GIF.',
    );
  }

  if (!asUserIsPremium && isPremiumImage) {
    throw new BadRequestException(
      'Brak dostępu do obrazów w formacie GIF. Wymagane członkostwo premium.',
    );
  }

  return imageUrl;
}

export function validateInstagramUrl(instagramUrl: string): string {
  if (instagramUrl.length == 0) {
    return instagramUrl;
  }

  const instagramPattern = /^https?:\/\/(?:www\.)?instagram\.com\/(.+)$/;

  if (!instagramPattern.test(instagramUrl)) {
    throw new BadRequestException(
      'Nieprawidłowy format linku Instagram.\nPoprawny format: https://www.instagram.com/[...].',
    );
  }

  return instagramUrl;
}

export function validateSoundCloudUrl(soundCloudUrl: string): string {
  if (soundCloudUrl.length == 0) {
    return soundCloudUrl;
  }

  const soundCloudPattern = /^https?:\/\/(?:www\.)?soundcloud\.com\/(.+)$/;

  if (!soundCloudPattern.test(soundCloudUrl)) {
    throw new BadRequestException(
      'Nieprawidłowy format linku SoundCloud.\nPoprawny format: https://soundcloud.com/[...].',
    );
  }

  return soundCloudUrl;
}

export function validateYouTubeUrl(youtubeUrl: string): string {
  if (youtubeUrl.length == 0) {
    return youtubeUrl;
  }

  const youtubePattern = /^https?:\/\/(?:www\.)?youtube\.com\/(.+)$/;

  if (!youtubePattern.test(youtubeUrl)) {
    throw new BadRequestException(
      'Nieprawidłowy format linku YouTube.\nPoprawny format: https://www.youtube.com/[...].',
    );
  }

  return youtubeUrl;
}

export function validateFacebookUrl(facebookUrl: string): string {
  if (facebookUrl.length == 0) {
    return facebookUrl;
  }

  const facebookPattern = /^https?:\/\/(?:www\.)?facebook\.com\/(.+)$/;

  if (!facebookPattern.test(facebookUrl)) {
    throw new BadRequestException(
      'Nieprawidłowy format linku Facebook.\nPoprawny format: https://www.facebook.com/[...].',
    );
  }

  return facebookUrl;
}

export function validateSpotifyUrl(spotifyUrl: string): string {
  if (spotifyUrl.length == 0) {
    return spotifyUrl;
  }

  const spotifyPattern = /^https?:\/\/(?:www\.)?spotify\.com\/(.+)$/;

  if (!spotifyPattern.test(spotifyUrl)) {
    throw new BadRequestException(
      'Nieprawidłowy format linku Spotify.\nPoprawny format: https://spotify.com/[...].',
    );
  }

  return spotifyUrl;
}
