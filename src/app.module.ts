import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AuthModule } from './api/auth/auth.module';
import { UsersModule } from './api/users/users.module';
import { RefreshSessionUserMiddleware } from './middleware/refresh-session-user.middleware';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { StorageModule } from './services/storage/storage.module';
import { AudioModule } from './api/audio/audio.module';
import { LogsModule } from './services/logger/logs.module';
import { ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';
import { NotificationsModule } from './services/notifications/notifications.module';
import { PermissionsModule } from './services/permissions/permissions.module';
import { TopicsModule } from './api/topics/topics.module';
import { TransactionsModule } from './api/transactions/transactions.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from 'database/data-source';
import { DefaultThrottlerGuard } from './throttler/default-throttler.guard';
import { OffersModule } from './api/offers/offers.module';

@Module({
  imports: [
    ThrottlerModule.forRoot([
      {
        ttl: 10000,
        limit: 10,
      },
    ]),
    ConfigModule.forRoot({ isGlobal: true }),
    AuthModule,
    UsersModule,
    AudioModule,
    TypeOrmModule.forRoot(dataSourceOptions),
    StorageModule,
    LogsModule,
    PermissionsModule,
    NotificationsModule,
    TopicsModule,
    TransactionsModule,
    OffersModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: DefaultThrottlerGuard,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RefreshSessionUserMiddleware).forRoutes('*');
  }
}
