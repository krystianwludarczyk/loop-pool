import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as Minio from 'minio';
import { v4 as uuid } from 'uuid';
import { Readable } from 'stream';

@Injectable()
export class StorageService {
  private readonly minio: Minio.Client;

  constructor(private readonly configService: ConfigService) {
    this.minio = new Minio.Client({
      endPoint: this.configService.getOrThrow('S3_ENDPOINT'),
      accessKey: this.configService.getOrThrow('S3_ACCESS_KEY_ID'),
      secretKey: this.configService.getOrThrow('S3_SECRET_ACCESS_KEY'),
    });
  }

  async uploadFile(file: Express.Multer.File, bucket: string): Promise<string> {
    try {
      const newKey = uuid();
      await this.minio.putObject(bucket, newKey, file.buffer, file.buffer.length, {
        'Content-Type': file.mimetype,
      });

      return newKey;
    } catch (error) {
      throw new InternalServerErrorException(`Błąd przesyłania pliku: ${error.message}`);
    }
  }

  async createReadStream(
    fileKey: string,
    bucket: string,
  ): Promise<Readable & { mimetype: string; length: number }> {
    try {
      const fileStream = await this.minio.getObject(bucket, fileKey);
      const fileStats = await this.minio.statObject(bucket, fileKey);
      const fileMimetype = fileStats.metaData['content-type'];
      return Object.assign(fileStream, { mimetype: fileMimetype, length: fileStats.size });
    } catch (error) {
      throw new InternalServerErrorException('Podczas pobierania pliku wystąpił błąd.');
    }
  }

  async deleteFile(fileKey: string, bucket: string): Promise<void> {
    try {
      await this.minio.removeObject(bucket, fileKey);
    } catch (error) {
      throw new InternalServerErrorException('Podczas usuwania pliku wystąpił błąd.');
    }
  }
}
