export enum ActionType {
  Create = 'Create',
  Read = 'Read',
  Edit = 'Edit',
  Manage = 'Manage',
  Delete = 'Delete',
  Mute = 'Mute',
  Ban = 'Ban',
  Pin = 'Pin',
}
