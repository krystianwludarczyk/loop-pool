import { ForbiddenException, Injectable } from '@nestjs/common';
import { Audio } from 'src/api/audio/entity/audio.entity';
import { Comment } from 'src/api/audio/entity/comment.entity';
import { User } from 'src/api/users/entity/user.entity';
import { ActionType } from './action.enum';
import { Topic } from 'src/api/topics/entity/topic.entity';
import { Reply } from 'src/api/topics/entity/reply.entity';
import { InsufficientPermissionsException } from 'src/exceptions/permissions/insufficient-permissions.exception';
import { UserMutedException } from 'src/exceptions/users/user-muted.exception';
import { AudioPrivateException } from 'src/exceptions/audio/audio-private.exception';
import { Offer } from 'src/api/offers/entity/offer.entity';
import { OfferExistsException } from 'src/exceptions/offers/offer-exists.exception';

@Injectable()
export class PermissionsService {
  async validateAudioAction(action: ActionType, audioData: Audio, asUserData: User): Promise<void> {
    switch (action) {
      case ActionType.Read:
        if (!audioData.isPrivate) {
          return;
        } else if (
          audioData.author.id == asUserData.id ||
          audioData.authorizedUsers.some((user) => user.id === asUserData.id)
        ) {
          return;
        }

        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new AudioPrivateException();
      case ActionType.Create:
        if (!asUserData.muteReason) {
          return;
        } else {
          throw new UserMutedException(asUserData.muteReason);
        }

      case ActionType.Edit:
        if (audioData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
      case ActionType.Delete:
        if (audioData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
    }
  }

  async validateUserAction(action: ActionType, userData: User, asUserData: User): Promise<void> {
    switch (action) {
      case ActionType.Edit:
        if (userData.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
      case ActionType.Manage:
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
      case ActionType.Mute:
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
      case ActionType.Ban:
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
    }
  }

  async validateCommentAction(
    action: ActionType,
    commentData: Comment,
    asUserData: User,
  ): Promise<void> {
    switch (action) {
      case ActionType.Create:
        if (!asUserData.muteReason) {
          return;
        } else {
          throw new UserMutedException(asUserData.muteReason);
        }

      case ActionType.Delete:
        if (commentData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
    }
  }

  async validateTopicAction(action: ActionType, topicData: Topic, asUserData: User): Promise<void> {
    switch (action) {
      case ActionType.Create:
        if (!asUserData.muteReason) {
          return;
        } else {
          throw new UserMutedException(asUserData.muteReason);
        }

      case ActionType.Edit:
        if (topicData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
      case ActionType.Delete:
        if (topicData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();

      case ActionType.Pin:
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
    }
  }

  async validateReplyAction(action: ActionType, replyData: Reply, asUserData: User): Promise<void> {
    switch (action) {
      case ActionType.Create:
        if (!asUserData.muteReason) {
          return;
        } else {
          throw new UserMutedException(asUserData.muteReason);
        }

      case ActionType.Delete:
        if (replyData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
    }
  }

  async validateOfferAction(action: ActionType, offerData: Offer, asUserData: User): Promise<void> {
    switch (action) {
      case ActionType.Create:
        if (asUserData.offer) {
          throw new OfferExistsException();
        }
        if (!asUserData.muteReason) {
          return;
        } else {
          throw new UserMutedException(asUserData.muteReason);
        }

      case ActionType.Edit:
        if (offerData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserModerator) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
      case ActionType.Delete:
        if (offerData.author.id === asUserData.id) {
          return;
        }
        if (asUserData.isUserAdmin) {
          return;
        }

        throw new InsufficientPermissionsException();
    }
  }
}
