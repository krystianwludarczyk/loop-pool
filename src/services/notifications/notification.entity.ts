import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Notification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  createdAt: Date;

  @Column()
  ownerId: number;

  @Column()
  open: boolean;

  @Column()
  content: string;

  @Column()
  redirectUrl: string;
}
