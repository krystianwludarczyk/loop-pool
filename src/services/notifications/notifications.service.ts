import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Notification } from './notification.entity';

@Injectable()
export class NotificationsService {
  constructor(
    @InjectRepository(Notification)
    private notificationsRepository: Repository<Notification>,
  ) {}

  async createNotification(redirectUrl: string, content: string, forUserId: number): Promise<void> {
    await this.notificationsRepository.save({
      createdAt: new Date(),
      ownerId: forUserId,
      content,
      redirectUrl,
      open: false,
    } as Notification);
  }

  async getUserNotifications(userId: number, count = 20): Promise<Notification[]> {
    const notifications = await this.notificationsRepository.find({
      where: { ownerId: userId },
      order: { createdAt: 'DESC' },
      take: count,
    });

    return notifications;
  }

  async getUserNewNotificationsCount(userId: number): Promise<number> {
    return await this.notificationsRepository.count({
      where: { ownerId: userId, open: false },
    });
  }

  async resetUserNotifications(userId: number): Promise<void> {
    const notifications = await this.notificationsRepository.find({
      where: { ownerId: userId, open: false },
    });

    notifications.forEach((notification) => {
      notification.open = true;
    });

    await this.notificationsRepository.save(notifications);
  }
}
