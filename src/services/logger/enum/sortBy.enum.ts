export enum SortBy {
  Oldest = 'Najstarsze',
  Newest = 'Najnowsze',
}
