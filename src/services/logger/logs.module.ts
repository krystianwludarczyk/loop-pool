import { Module } from '@nestjs/common';
import { LogsService } from './logs.service';
import { Log } from './log.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Log])],
  exports: [LogsService],
  providers: [LogsService],
})
export class LogsModule {}
