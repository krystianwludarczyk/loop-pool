import { Injectable } from '@nestjs/common';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { Log } from './log.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { SortBy } from './enum/sortBy.enum';

@Injectable()
export class LogsService {
  constructor(
    @InjectRepository(Log)
    private logsRepository: Repository<Log>,
  ) {}

  createConsoleLog(content: string): void {
    const now = new Date();
    const nowString = now.toLocaleDateString('pl-PL', {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
    });

    console.log(`${nowString} > ${content}`);
  }

  async createLog(content: string, before: string, after: string): Promise<void> {
    await this.logsRepository.save({
      createdAt: new Date(),
      content,
      before,
      after,
    });
  }

  async getLastLogs(count: number): Promise<Log[]> {
    const logs = await this.logsRepository.find({
      order: {
        createdAt: 'DESC',
      },
      take: count,
    });

    return logs;
  }

  async getByQuery(
    query: Record<string, any>,
    page = 1,
    pageSize = 20,
  ): Promise<{ logsData: Log[]; totalPages: number }> {
    let queryBuilder: SelectQueryBuilder<Log> = this.logsRepository.createQueryBuilder('log');

    switch (query.sortBy) {
      case SortBy.Newest:
        queryBuilder = queryBuilder.orderBy('log.createdAt', 'DESC');
        break;
      case SortBy.Oldest:
        queryBuilder = queryBuilder.orderBy('log.createdAt', 'ASC');
        break;
      default:
        queryBuilder = queryBuilder.orderBy('log.createdAt', 'DESC');
        break;
    }

    Object.entries(query).forEach(([key, value]) => {
      switch (key) {
        case 'keywords':
          queryBuilder = queryBuilder.andWhere('log.content LIKE :keywords', {
            keywords: `%${value}%`,
          });
          break;
      }
    });

    const totalResults: number = await queryBuilder.getCount();
    const totalPages: number = Math.ceil(totalResults / pageSize);
    queryBuilder.skip((page - 1) * pageSize).take(pageSize);
    const logsData: Log[] = await queryBuilder.getMany();

    return { logsData, totalPages };
  }
}
