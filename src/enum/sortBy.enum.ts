export enum SortBy {
  Oldest = 'Najstarsze',
  Newest = 'Najnowsze',
  AZ = 'A-Z',
  ZA = 'Z-A',
}
