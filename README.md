# Loop Pool

## Requirements

- Node 20.10.0
- npm 10.2.3

## Installlation

```bash
cp .env.example .env
npm i
npm run migration:run
```

## Running - Local

1. Set correct .env configuration
2. Start the suite

```bash
npm run start:dev
```

3. App should be available on [http://localhost:3000](http://localhost:3000)

## Migrations

### Generate migration

If you want to automatically detect modifications in the schema and generate migration to reflect those changes, you should use the following command:

```bash
npm run migration:generate -- database/migrations/ExampleName
```

This will create a new migration file in the `database/migrations/` directory.

### Executing migrations

To execute a migration and apply the changes to your database, you can use the following command:

```bash
npm run migration:run
```

This will run all pending migrations that have not yet been applied to the database.
