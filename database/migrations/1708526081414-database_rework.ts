import { MigrationInterface, QueryRunner } from "typeorm";

export class DatabaseRework1708526081414 implements MigrationInterface {
    name = 'DatabaseRework1708526081414'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_comment" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "audioParentId" integer, CONSTRAINT "FK_276779da446413a0d79598d4fbd" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_comment"("id", "content", "createdAt", "authorId", "audioParentId") SELECT "id", "content", "createdAt", "authorId", "audioParentId" FROM "comment"`);
        await queryRunner.query(`DROP TABLE "comment"`);
        await queryRunner.query(`ALTER TABLE "temporary_comment" RENAME TO "comment"`);
        await queryRunner.query(`CREATE TABLE "temporary_audio" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "imageUrl" varchar NOT NULL, "createdAt" datetime NOT NULL, "title" varchar NOT NULL, "description" varchar NOT NULL, "fileKey" varchar NOT NULL, "category" varchar NOT NULL, "bpm" integer NOT NULL, "key" varchar NOT NULL, "keyType" varchar NOT NULL, "authorId" integer, CONSTRAINT "FK_c3a0861fcfc6e7b96937b73670d" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_audio"("id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId") SELECT "id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId" FROM "audio"`);
        await queryRunner.query(`DROP TABLE "audio"`);
        await queryRunner.query(`ALTER TABLE "temporary_audio" RENAME TO "audio"`);
        await queryRunner.query(`CREATE TABLE "temporary_comment" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "audioParentId" integer, CONSTRAINT "FK_276779da446413a0d79598d4fbd" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_361e0c3ab353eb7b7fa697b0739" FOREIGN KEY ("audioParentId") REFERENCES "audio" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_comment"("id", "content", "createdAt", "authorId", "audioParentId") SELECT "id", "content", "createdAt", "authorId", "audioParentId" FROM "comment"`);
        await queryRunner.query(`DROP TABLE "comment"`);
        await queryRunner.query(`ALTER TABLE "temporary_comment" RENAME TO "comment"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "comment" RENAME TO "temporary_comment"`);
        await queryRunner.query(`CREATE TABLE "comment" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "audioParentId" integer, CONSTRAINT "FK_276779da446413a0d79598d4fbd" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "comment"("id", "content", "createdAt", "authorId", "audioParentId") SELECT "id", "content", "createdAt", "authorId", "audioParentId" FROM "temporary_comment"`);
        await queryRunner.query(`DROP TABLE "temporary_comment"`);
        await queryRunner.query(`ALTER TABLE "audio" RENAME TO "temporary_audio"`);
        await queryRunner.query(`CREATE TABLE "audio" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "imageUrl" varchar NOT NULL, "isPremium" boolean NOT NULL, "createdAt" datetime NOT NULL, "title" varchar NOT NULL, "description" varchar NOT NULL, "fileKey" varchar NOT NULL, "category" varchar NOT NULL, "bpm" integer NOT NULL, "key" varchar NOT NULL, "keyType" varchar NOT NULL, "authorId" integer, CONSTRAINT "FK_c3a0861fcfc6e7b96937b73670d" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "audio"("id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId") SELECT "id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId" FROM "temporary_audio"`);
        await queryRunner.query(`DROP TABLE "temporary_audio"`);
        await queryRunner.query(`ALTER TABLE "comment" RENAME TO "temporary_comment"`);
        await queryRunner.query(`CREATE TABLE "comment" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "audioParentId" integer, CONSTRAINT "FK_361e0c3ab353eb7b7fa697b0739" FOREIGN KEY ("audioParentId") REFERENCES "audio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_276779da446413a0d79598d4fbd" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "comment"("id", "content", "createdAt", "authorId", "audioParentId") SELECT "id", "content", "createdAt", "authorId", "audioParentId" FROM "temporary_comment"`);
        await queryRunner.query(`DROP TABLE "temporary_comment"`);
    }

}
