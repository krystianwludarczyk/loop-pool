import { MigrationInterface, QueryRunner } from "typeorm";

export class Offercategory1734370250336 implements MigrationInterface {
    name = 'Offercategory1734370250336'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_offer" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "category" varchar NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL)`);
        await queryRunner.query(`INSERT INTO "temporary_offer"("id", "category", "content", "createdAt") SELECT "id", "title", "content", "createdAt" FROM "offer"`);
        await queryRunner.query(`DROP TABLE "offer"`);
        await queryRunner.query(`ALTER TABLE "temporary_offer" RENAME TO "offer"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "offer" RENAME TO "temporary_offer"`);
        await queryRunner.query(`CREATE TABLE "offer" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL)`);
        await queryRunner.query(`INSERT INTO "offer"("id", "title", "content", "createdAt") SELECT "id", "category", "content", "createdAt" FROM "temporary_offer"`);
        await queryRunner.query(`DROP TABLE "temporary_offer"`);
    }

}
