import { MigrationInterface, QueryRunner } from "typeorm";

export class Transactions21708201638800 implements MigrationInterface {
    name = 'Transactions21708201638800'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_transaction" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "price" integer NOT NULL, "status" varchar NOT NULL, "userId" integer, "title" varchar NOT NULL DEFAULT (''), "paymentId" varchar, "benefitsDone" boolean NOT NULL DEFAULT (0), "createdAt" datetime, CONSTRAINT "FK_605baeb040ff0fae995404cea37" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_transaction"("id", "price", "status", "userId", "title", "paymentId", "benefitsDone") SELECT "id", "price", "status", "userId", "title", "paymentId", "benefitsDone" FROM "transaction"`);
        await queryRunner.query(`DROP TABLE "transaction"`);
        await queryRunner.query(`ALTER TABLE "temporary_transaction" RENAME TO "transaction"`);
        await queryRunner.query(`CREATE TABLE "temporary_transaction" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "price" integer NOT NULL, "status" varchar NOT NULL, "userId" integer, "title" varchar, "paymentId" varchar, "benefitsDone" boolean NOT NULL DEFAULT (0), "createdAt" datetime, CONSTRAINT "FK_605baeb040ff0fae995404cea37" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_transaction"("id", "price", "status", "userId", "title", "paymentId", "benefitsDone", "createdAt") SELECT "id", "price", "status", "userId", "title", "paymentId", "benefitsDone", "createdAt" FROM "transaction"`);
        await queryRunner.query(`DROP TABLE "transaction"`);
        await queryRunner.query(`ALTER TABLE "temporary_transaction" RENAME TO "transaction"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" RENAME TO "temporary_transaction"`);
        await queryRunner.query(`CREATE TABLE "transaction" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "price" integer NOT NULL, "status" varchar NOT NULL, "userId" integer, "title" varchar NOT NULL DEFAULT (''), "paymentId" varchar, "benefitsDone" boolean NOT NULL DEFAULT (0), "createdAt" datetime, CONSTRAINT "FK_605baeb040ff0fae995404cea37" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "transaction"("id", "price", "status", "userId", "title", "paymentId", "benefitsDone", "createdAt") SELECT "id", "price", "status", "userId", "title", "paymentId", "benefitsDone", "createdAt" FROM "temporary_transaction"`);
        await queryRunner.query(`DROP TABLE "temporary_transaction"`);
        await queryRunner.query(`ALTER TABLE "transaction" RENAME TO "temporary_transaction"`);
        await queryRunner.query(`CREATE TABLE "transaction" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "price" integer NOT NULL, "status" varchar NOT NULL, "userId" integer, "title" varchar NOT NULL DEFAULT (''), "paymentId" varchar, "benefitsDone" boolean NOT NULL DEFAULT (0), CONSTRAINT "FK_605baeb040ff0fae995404cea37" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "transaction"("id", "price", "status", "userId", "title", "paymentId", "benefitsDone") SELECT "id", "price", "status", "userId", "title", "paymentId", "benefitsDone" FROM "temporary_transaction"`);
        await queryRunner.query(`DROP TABLE "temporary_transaction"`);
    }

}
