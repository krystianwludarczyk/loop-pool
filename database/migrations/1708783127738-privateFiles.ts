import { MigrationInterface, QueryRunner } from "typeorm";

export class PrivateFiles1708783127738 implements MigrationInterface {
    name = 'PrivateFiles1708783127738'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user_allowedAudio" ("user_id" integer NOT NULL, "file_id" integer NOT NULL, PRIMARY KEY ("user_id", "file_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_ce91bca35835dfcb7f3fcf6574" ON "user_allowedAudio" ("user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_c83e8f9ad190ecf3a94ff22a66" ON "user_allowedAudio" ("file_id") `);
        await queryRunner.query(`CREATE TABLE "temporary_audio" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "imageUrl" varchar NOT NULL, "createdAt" datetime NOT NULL, "title" varchar NOT NULL, "description" varchar NOT NULL, "fileKey" varchar NOT NULL, "category" varchar NOT NULL, "bpm" integer NOT NULL, "key" varchar NOT NULL, "keyType" varchar NOT NULL, "authorId" integer, "isPrivate" boolean NOT NULL DEFAULT (0), CONSTRAINT "FK_c3a0861fcfc6e7b96937b73670d" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_audio"("id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId") SELECT "id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId" FROM "audio"`);
        await queryRunner.query(`DROP TABLE "audio"`);
        await queryRunner.query(`ALTER TABLE "temporary_audio" RENAME TO "audio"`);
        await queryRunner.query(`DROP INDEX "IDX_ce91bca35835dfcb7f3fcf6574"`);
        await queryRunner.query(`DROP INDEX "IDX_c83e8f9ad190ecf3a94ff22a66"`);
        await queryRunner.query(`CREATE TABLE "temporary_user_allowedAudio" ("user_id" integer NOT NULL, "file_id" integer NOT NULL, CONSTRAINT "FK_ce91bca35835dfcb7f3fcf65744" FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE, CONSTRAINT "FK_c83e8f9ad190ecf3a94ff22a669" FOREIGN KEY ("file_id") REFERENCES "audio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, PRIMARY KEY ("user_id", "file_id"))`);
        await queryRunner.query(`INSERT INTO "temporary_user_allowedAudio"("user_id", "file_id") SELECT "user_id", "file_id" FROM "user_allowedAudio"`);
        await queryRunner.query(`DROP TABLE "user_allowedAudio"`);
        await queryRunner.query(`ALTER TABLE "temporary_user_allowedAudio" RENAME TO "user_allowedAudio"`);
        await queryRunner.query(`CREATE INDEX "IDX_ce91bca35835dfcb7f3fcf6574" ON "user_allowedAudio" ("user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_c83e8f9ad190ecf3a94ff22a66" ON "user_allowedAudio" ("file_id") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_c83e8f9ad190ecf3a94ff22a66"`);
        await queryRunner.query(`DROP INDEX "IDX_ce91bca35835dfcb7f3fcf6574"`);
        await queryRunner.query(`ALTER TABLE "user_allowedAudio" RENAME TO "temporary_user_allowedAudio"`);
        await queryRunner.query(`CREATE TABLE "user_allowedAudio" ("user_id" integer NOT NULL, "file_id" integer NOT NULL, PRIMARY KEY ("user_id", "file_id"))`);
        await queryRunner.query(`INSERT INTO "user_allowedAudio"("user_id", "file_id") SELECT "user_id", "file_id" FROM "temporary_user_allowedAudio"`);
        await queryRunner.query(`DROP TABLE "temporary_user_allowedAudio"`);
        await queryRunner.query(`CREATE INDEX "IDX_c83e8f9ad190ecf3a94ff22a66" ON "user_allowedAudio" ("file_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_ce91bca35835dfcb7f3fcf6574" ON "user_allowedAudio" ("user_id") `);
        await queryRunner.query(`ALTER TABLE "audio" RENAME TO "temporary_audio"`);
        await queryRunner.query(`CREATE TABLE "audio" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "imageUrl" varchar NOT NULL, "createdAt" datetime NOT NULL, "title" varchar NOT NULL, "description" varchar NOT NULL, "fileKey" varchar NOT NULL, "category" varchar NOT NULL, "bpm" integer NOT NULL, "key" varchar NOT NULL, "keyType" varchar NOT NULL, "authorId" integer, CONSTRAINT "FK_c3a0861fcfc6e7b96937b73670d" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "audio"("id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId") SELECT "id", "imageUrl", "createdAt", "title", "description", "fileKey", "category", "bpm", "key", "keyType", "authorId" FROM "temporary_audio"`);
        await queryRunner.query(`DROP TABLE "temporary_audio"`);
        await queryRunner.query(`DROP INDEX "IDX_c83e8f9ad190ecf3a94ff22a66"`);
        await queryRunner.query(`DROP INDEX "IDX_ce91bca35835dfcb7f3fcf6574"`);
        await queryRunner.query(`DROP TABLE "user_allowedAudio"`);
    }

}
