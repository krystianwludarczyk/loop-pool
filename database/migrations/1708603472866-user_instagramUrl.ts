import { MigrationInterface, QueryRunner } from "typeorm";

export class UserInstagramUrl1708603472866 implements MigrationInterface {
    name = 'UserInstagramUrl1708603472866'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "username" varchar NOT NULL, "discordId" varchar NOT NULL, "showDiscordProfile" boolean NOT NULL, "email" varchar NOT NULL, "isUserAdmin" boolean NOT NULL, "isUserModerator" boolean NOT NULL, "muteReason" varchar, "banReason" varchar, "registredAt" datetime NOT NULL, "lastOnline" datetime NOT NULL, "showLastOnline" boolean NOT NULL, "avatarUrl" varchar NOT NULL, "aboutMe" varchar NOT NULL, "premiumPlanExpiration" datetime NOT NULL, "instagramUrl" varchar)`);
        await queryRunner.query(`INSERT INTO "temporary_user"("id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration") SELECT "id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration" FROM "user"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`ALTER TABLE "temporary_user" RENAME TO "user"`);
        await queryRunner.query(`CREATE TABLE "temporary_user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "username" varchar NOT NULL, "discordId" varchar NOT NULL, "showDiscordProfile" boolean NOT NULL, "email" varchar NOT NULL, "isUserAdmin" boolean NOT NULL, "isUserModerator" boolean NOT NULL, "muteReason" varchar, "banReason" varchar, "registredAt" datetime NOT NULL, "lastOnline" datetime NOT NULL, "showLastOnline" boolean NOT NULL, "avatarUrl" varchar, "aboutMe" varchar, "premiumPlanExpiration" datetime NOT NULL, "instagramUrl" varchar)`);
        await queryRunner.query(`INSERT INTO "temporary_user"("id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration", "instagramUrl") SELECT "id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration", "instagramUrl" FROM "user"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`ALTER TABLE "temporary_user" RENAME TO "user"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" RENAME TO "temporary_user"`);
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "username" varchar NOT NULL, "discordId" varchar NOT NULL, "showDiscordProfile" boolean NOT NULL, "email" varchar NOT NULL, "isUserAdmin" boolean NOT NULL, "isUserModerator" boolean NOT NULL, "muteReason" varchar, "banReason" varchar, "registredAt" datetime NOT NULL, "lastOnline" datetime NOT NULL, "showLastOnline" boolean NOT NULL, "avatarUrl" varchar NOT NULL, "aboutMe" varchar NOT NULL, "premiumPlanExpiration" datetime NOT NULL, "instagramUrl" varchar)`);
        await queryRunner.query(`INSERT INTO "user"("id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration", "instagramUrl") SELECT "id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration", "instagramUrl" FROM "temporary_user"`);
        await queryRunner.query(`DROP TABLE "temporary_user"`);
        await queryRunner.query(`ALTER TABLE "user" RENAME TO "temporary_user"`);
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "username" varchar NOT NULL, "discordId" varchar NOT NULL, "showDiscordProfile" boolean NOT NULL, "email" varchar NOT NULL, "isUserAdmin" boolean NOT NULL, "isUserModerator" boolean NOT NULL, "muteReason" varchar, "banReason" varchar, "registredAt" datetime NOT NULL, "lastOnline" datetime NOT NULL, "showLastOnline" boolean NOT NULL, "avatarUrl" varchar NOT NULL, "aboutMe" varchar NOT NULL, "premiumPlanExpiration" datetime NOT NULL)`);
        await queryRunner.query(`INSERT INTO "user"("id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration") SELECT "id", "username", "discordId", "showDiscordProfile", "email", "isUserAdmin", "isUserModerator", "muteReason", "banReason", "registredAt", "lastOnline", "showLastOnline", "avatarUrl", "aboutMe", "premiumPlanExpiration" FROM "temporary_user"`);
        await queryRunner.query(`DROP TABLE "temporary_user"`);
    }

}
