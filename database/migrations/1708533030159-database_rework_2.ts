import { MigrationInterface, QueryRunner } from "typeorm";

export class DatabaseRework21708533030159 implements MigrationInterface {
    name = 'DatabaseRework21708533030159'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_reply" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "topicParentId" integer, CONSTRAINT "FK_9c7aa85b4b2be67c1b7235d03fe" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_reply"("id", "content", "createdAt", "authorId", "topicParentId") SELECT "id", "content", "createdAt", "authorId", "topicParentId" FROM "reply"`);
        await queryRunner.query(`DROP TABLE "reply"`);
        await queryRunner.query(`ALTER TABLE "temporary_reply" RENAME TO "reply"`);
        await queryRunner.query(`CREATE TABLE "temporary_reply" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "topicParentId" integer, CONSTRAINT "FK_9c7aa85b4b2be67c1b7235d03fe" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_96904e14293d02d0148892a5cfc" FOREIGN KEY ("topicParentId") REFERENCES "topic" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_reply"("id", "content", "createdAt", "authorId", "topicParentId") SELECT "id", "content", "createdAt", "authorId", "topicParentId" FROM "reply"`);
        await queryRunner.query(`DROP TABLE "reply"`);
        await queryRunner.query(`ALTER TABLE "temporary_reply" RENAME TO "reply"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "reply" RENAME TO "temporary_reply"`);
        await queryRunner.query(`CREATE TABLE "reply" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "topicParentId" integer, CONSTRAINT "FK_9c7aa85b4b2be67c1b7235d03fe" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "reply"("id", "content", "createdAt", "authorId", "topicParentId") SELECT "id", "content", "createdAt", "authorId", "topicParentId" FROM "temporary_reply"`);
        await queryRunner.query(`DROP TABLE "temporary_reply"`);
        await queryRunner.query(`ALTER TABLE "reply" RENAME TO "temporary_reply"`);
        await queryRunner.query(`CREATE TABLE "reply" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "content" varchar NOT NULL, "createdAt" datetime NOT NULL, "authorId" integer, "topicParentId" integer, CONSTRAINT "FK_96904e14293d02d0148892a5cfc" FOREIGN KEY ("topicParentId") REFERENCES "topic" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_9c7aa85b4b2be67c1b7235d03fe" FOREIGN KEY ("authorId") REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "reply"("id", "content", "createdAt", "authorId", "topicParentId") SELECT "id", "content", "createdAt", "authorId", "topicParentId" FROM "temporary_reply"`);
        await queryRunner.query(`DROP TABLE "temporary_reply"`);
    }

}
