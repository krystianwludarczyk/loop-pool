function createDeleteTopicPopup(topicId, content) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Usuń temat';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');

  const textParagraph = document.createElement('h3');
  textParagraph.classList.add('light-gray', 'width100');
  textParagraph.textContent = 'Jesteś pewien, że chcesz usunąć ten temat?';

  const contentParagraph = document.createElement('p');
  contentParagraph.classList.add('light-gray', 'width100');
  contentParagraph.textContent = content;

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';

  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('default-button');
  deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';

  deleteButton.addEventListener('click', async () => {
    deleteButton.setAttribute('disabled', '');
    deleteButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const response = await fetch(`/topics/${topicId}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.href = `/`;
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      deleteButton.removeAttribute('disabled');
      deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(deleteButton);

  popup.appendChild(headerWrapper);
  contentWrapper.appendChild(textParagraph);
  contentWrapper.appendChild(contentParagraph);
  popup.appendChild(contentWrapper);
  popup.appendChild(buttonWrapper);
  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createDeleteAudioPopup(audioId, content) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Usuń plik';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');

  const textParagraph = document.createElement('h3');
  textParagraph.classList.add('light-gray', 'width100');
  textParagraph.textContent = 'Jesteś pewien, że chcesz usunąć ten plik?';

  const contentParagraph = document.createElement('p');
  contentParagraph.classList.add('light-gray', 'width100');
  contentParagraph.textContent = content;

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';

  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('default-button');
  deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';

  deleteButton.addEventListener('click', async () => {
    deleteButton.setAttribute('disabled', '');
    deleteButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const response = await fetch(`/audio/${audioId}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.href = `/`;
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      deleteButton.removeAttribute('disabled');
      deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(deleteButton);

  popup.appendChild(headerWrapper);
  contentWrapper.appendChild(textParagraph);
  contentWrapper.appendChild(contentParagraph);
  popup.appendChild(contentWrapper);
  popup.appendChild(buttonWrapper);
  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createCreateTopicPopup() {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');
  popup.appendChild(headerWrapper);

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Utwórz nowy temat';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');
  popup.appendChild(contentWrapper);

  const titleHeading = document.createElement('h3');
  titleHeading.classList.add('white', 'width100');
  titleHeading.textContent = 'Tytuł';
  contentWrapper.appendChild(titleHeading);

  const titleInput = document.createElement('input');
  titleInput.classList.add('form-input', 'padding10', 'width100');
  titleInput.type = 'text';
  contentWrapper.appendChild(titleInput);

  const contentHeading = document.createElement('h3');
  contentHeading.classList.add('white', 'width100', 'margin-top');
  contentHeading.textContent = 'Treść';
  contentWrapper.appendChild(contentHeading);

  const contentTextarea = document.createElement('textarea');
  contentTextarea.classList.add('form-input-textarea', 'width100');
  contentTextarea.type = 'text';
  contentTextarea.rows = '10';
  contentWrapper.appendChild(contentTextarea);

  const followWrapper = document.createElement('div');
  followWrapper.classList.add('space-between-row-wrapper', 'margin-top', 'width100');

  const followHeading = document.createElement('h3');
  followHeading.classList.add('white');
  followHeading.textContent = 'Obserwuj';
  followWrapper.appendChild(followHeading);

  const followInput = document.createElement('input');
  followInput.classList.add('form-input-checkbox');
  followInput.type = 'checkbox';
  followInput.checked = true;
  followWrapper.appendChild(followInput);

  contentWrapper.appendChild(followWrapper);

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';
  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const createButton = document.createElement('button');
  createButton.classList.add('default-button');
  createButton.innerHTML = 'Utwórz<span class="material-symbols-outlined">add</span>';

  createButton.addEventListener('click', async () => {
    createButton.setAttribute('disabled', '');
    createButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const title = titleInput.value;
    const content = contentTextarea.value;
    const follow = followInput.checked;

    const rawData = {
      title: title,
      content: content,
      follow: follow,
    };
    const dataToSend = JSON.stringify(rawData);

    const response = await fetch(`/topics`, {
      method: 'POST',
      body: dataToSend,
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      const data = await response.json();
      window.location.href = `/topics/${data.id}`;
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      createButton.removeAttribute('disabled');
      createButton.innerHTML = 'Utwórz<span class="material-symbols-outlined">add</span>';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(createButton);
  popup.appendChild(buttonWrapper);

  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createEditTopicPopup(topicId, title, content) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');
  popup.appendChild(headerWrapper);

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Edytuj temat';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');
  popup.appendChild(contentWrapper);

  const titleHeading = document.createElement('h3');
  titleHeading.classList.add('white', 'width100');
  titleHeading.textContent = 'Tytuł';
  contentWrapper.appendChild(titleHeading);

  const titleInput = document.createElement('input');
  titleInput.classList.add('form-input', 'padding10', 'width100');
  titleInput.type = 'text';
  titleInput.value = title;
  contentWrapper.appendChild(titleInput);

  const contentHeading = document.createElement('h3');
  contentHeading.classList.add('white', 'width100', 'margin-top');
  contentHeading.textContent = 'Treść';
  contentWrapper.appendChild(contentHeading);

  const contentTextarea = document.createElement('textarea');
  contentTextarea.classList.add('form-input-textarea', 'width100');
  contentTextarea.type = 'text';
  contentTextarea.rows = '10';
  contentTextarea.innerHTML = content;
  contentWrapper.appendChild(contentTextarea);

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';
  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const createButton = document.createElement('button');
  createButton.classList.add('default-button');
  createButton.innerHTML = 'Zapisz<span class="material-symbols-outlined">done</span>';

  createButton.addEventListener('click', async () => {
    createButton.setAttribute('disabled', '');
    createButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const title = titleInput.value;
    const content = contentTextarea.value;

    const rawData = {
      title: title,
      content: content,
    };
    const dataToSend = JSON.stringify(rawData);

    const response = await fetch(`/topics/${topicId}`, {
      method: 'PATCH',
      body: dataToSend,
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      const data = await response.json();
      window.location.href = `/topics/${data.id}`;
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      createButton.removeAttribute('disabled');
      createButton.innerHTML = 'Zapisz<span class="material-symbols-outlined">done</span>';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(createButton);
  popup.appendChild(buttonWrapper);

  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createBanUserPopup(userId, username) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');
  popup.appendChild(headerWrapper);

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = `Zbanuj ${username}`;
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');
  popup.appendChild(contentWrapper);

  const reasonHeading = document.createElement('h3');
  reasonHeading.classList.add('white', 'width100');
  reasonHeading.textContent = 'Powód';
  contentWrapper.appendChild(reasonHeading);

  const reasonInput = document.createElement('input');
  reasonInput.classList.add('form-input', 'padding10', 'width100');
  reasonInput.type = 'text';
  contentWrapper.appendChild(reasonInput);

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';
  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const banButton = document.createElement('button');
  banButton.classList.add('default-button');
  banButton.innerHTML = `Zbanuj<span class="material-symbols-outlined">lock</span>`;

  banButton.addEventListener('click', async () => {
    banButton.setAttribute('disabled', '');
    banButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const reason = reasonInput.value;

    const rawData = {
      reason: reason,
    };
    const dataToSend = JSON.stringify(rawData);

    const response = await fetch(`/users/${userId}/ban`, {
      method: 'POST',
      body: dataToSend,
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.reload();
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      banButton.removeAttribute('disabled');
      banButton.innerHTML = `Zbanuj<span class="material-symbols-outlined">lock</span>`;
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(banButton);
  popup.appendChild(buttonWrapper);

  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createUnbanUserPopup(userId, username, reason) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = `Odbanuj ${username}`;
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');

  const reasonHeading = document.createElement('h3');
  reasonHeading.classList.add('white', 'width100');
  reasonHeading.textContent = 'Powód';
  contentWrapper.appendChild(reasonHeading);

  const contentParagraph = document.createElement('p');
  contentParagraph.classList.add('light-gray', 'width100');
  contentParagraph.textContent = reason;

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';

  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('default-button');
  deleteButton.innerHTML = '<span class="material-symbols-outlined">lock_open</span>Odbanuj';

  deleteButton.addEventListener('click', async () => {
    deleteButton.setAttribute('disabled', '');
    deleteButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const response = await fetch(`/users/${userId}/unban`, {
      method: 'POST',
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.reload();
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      deleteButton.removeAttribute('disabled');
      deleteButton.innerHTML = '<span class="material-symbols-outlined">lock_open</span>Odbanuj';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(deleteButton);

  popup.appendChild(headerWrapper);
  contentWrapper.appendChild(contentParagraph);
  popup.appendChild(contentWrapper);
  popup.appendChild(buttonWrapper);
  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createMuteUserPopup(userId, username) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');
  popup.appendChild(headerWrapper);

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = `Wycisz ${username}`;
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');
  popup.appendChild(contentWrapper);

  const reasonHeading = document.createElement('h3');
  reasonHeading.classList.add('white', 'width100');
  reasonHeading.textContent = 'Powód';
  contentWrapper.appendChild(reasonHeading);

  const reasonInput = document.createElement('input');
  reasonInput.classList.add('form-input', 'padding10', 'width100');
  reasonInput.type = 'text';
  contentWrapper.appendChild(reasonInput);

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';
  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const banButton = document.createElement('button');
  banButton.classList.add('default-button');
  banButton.innerHTML = `Wycisz<span class="material-symbols-outlined">block</span>`;

  banButton.addEventListener('click', async () => {
    banButton.setAttribute('disabled', '');
    banButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const reason = reasonInput.value;

    const rawData = {
      reason: reason,
    };
    const dataToSend = JSON.stringify(rawData);

    const response = await fetch(`/users/${userId}/mute`, {
      method: 'POST',
      body: dataToSend,
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.reload();
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      banButton.removeAttribute('disabled');
      banButton.innerHTML = `Wycisz<span class="material-symbols-outlined">block</span>`;
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(banButton);
  popup.appendChild(buttonWrapper);

  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createUnmuteUserPopup(userId, username, reason) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = `Odcisz ${username}`;
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');

  const reasonHeading = document.createElement('h3');
  reasonHeading.classList.add('white', 'width100');
  reasonHeading.textContent = 'Powód';
  contentWrapper.appendChild(reasonHeading);

  const contentParagraph = document.createElement('p');
  contentParagraph.classList.add('light-gray', 'width100');
  contentParagraph.textContent = reason;

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';

  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('default-button');
  deleteButton.innerHTML = '<span class="material-symbols-outlined">sync</span>Odcisz';

  deleteButton.addEventListener('click', async () => {
    deleteButton.setAttribute('disabled', '');
    deleteButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const response = await fetch(`/users/${userId}/unmute`, {
      method: 'POST',
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.reload();
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      deleteButton.removeAttribute('disabled');
      deleteButton.innerHTML = '<span class="material-symbols-outlined">sync</span>Odcisz';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(deleteButton);

  popup.appendChild(headerWrapper);
  contentWrapper.appendChild(contentParagraph);
  popup.appendChild(contentWrapper);
  popup.appendChild(buttonWrapper);
  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createDeleteReplyPopup(topicId, replyId, content) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Usuń odpowiedź';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');

  const textParagraph = document.createElement('h3');
  textParagraph.classList.add('light-gray', 'width100');
  textParagraph.textContent = 'Jesteś pewien, że chcesz usunąć tę odpowiedź?';

  const contentParagraph = document.createElement('p');
  contentParagraph.classList.add('light-gray', 'width100');
  contentParagraph.textContent = content;

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';

  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('default-button');
  deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';

  deleteButton.addEventListener('click', async () => {
    deleteButton.setAttribute('disabled', '');
    deleteButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const response = await fetch(`/replies/${replyId}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.reload();
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      deleteButton.removeAttribute('disabled');
      deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(deleteButton);

  popup.appendChild(headerWrapper);
  contentWrapper.appendChild(textParagraph);
  contentWrapper.appendChild(contentParagraph);
  popup.appendChild(contentWrapper);
  popup.appendChild(buttonWrapper);
  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createDeleteCommentPopup(commentId, content) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Usuń komentarz';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');

  const textParagraph = document.createElement('h3');
  textParagraph.classList.add('light-gray', 'width100');
  textParagraph.textContent = 'Jesteś pewien, że chcesz usunąć ten komentarz?';

  const contentParagraph = document.createElement('p');
  contentParagraph.classList.add('light-gray', 'width100');
  contentParagraph.textContent = content;

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';

  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('default-button');
  deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';

  deleteButton.addEventListener('click', async () => {
    deleteButton.setAttribute('disabled', '');
    deleteButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const response = await fetch(`/comments/${commentId}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.reload();
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      deleteButton.removeAttribute('disabled');
      deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(deleteButton);

  popup.appendChild(headerWrapper);
  contentWrapper.appendChild(textParagraph);
  contentWrapper.appendChild(contentParagraph);
  popup.appendChild(contentWrapper);
  popup.appendChild(buttonWrapper);
  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/******************************************************************************* */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
function createAlert(color, message, annotation = null, redirectTo = null) {
  document
    .querySelectorAll('.alert-box-red, .alert-box-green, .alert-box-gray')
    .forEach((alert) => {
      alert.remove();
    });

  let alertElement;
  if (redirectTo) {
    alertElement = document.createElement('a');
    alertElement.href = redirectTo;
  } else {
    alertElement = document.createElement('div');
  }

  if (color == 'red') {
    alertElement.className = 'alert-box-red';
  } else if (color == 'green') {
    alertElement.className = 'alert-box-green';
  } else if (color == 'gray') {
    alertElement.className = 'alert-box-gray';
  }

  var paraElement = document.createElement('p');
  paraElement.className = 'width100';
  paraElement.innerHTML = message;
  alertElement.appendChild(paraElement);

  if (annotation) {
    var annotationElement = document.createElement('p');
    annotationElement.className = 'width100 text-smaller';
    annotationElement.innerHTML = annotation;
    alertElement.appendChild(annotationElement);
  }

  document.body.appendChild(alertElement);
  setTimeout(function () {
    if (document.body.contains(alertElement)) {
      document.body.removeChild(alertElement);
    }
  }, 3000);
}

function resetAllAudio() {
  const allAudioPlayers = document.querySelectorAll('audio');
  allAudioPlayers.forEach((player) => {
    player.pause();
    const playerId = player.id.split('-')[1];
    const playerImg = document.getElementById(`audioImg-${playerId}`);
    const playerPlaybackStatus = document.getElementById(`playbackStatus-${playerId}`);

    if (playerImg && playerPlaybackStatus) {
      playerImg.classList.remove('audio-box-img-play');
      playerPlaybackStatus.innerHTML = '<span class="material-symbols-outlined">play_arrow</span>';
    }
  });
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createCreateOfferPopup() {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');
  popup.appendChild(headerWrapper);

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Utwórz nową ofertę';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');
  popup.appendChild(contentWrapper);

  const categoryHeading = document.createElement('h3');
  categoryHeading.classList.add('white', 'width100');
  categoryHeading.textContent = 'Kategoria';
  contentWrapper.appendChild(categoryHeading);

  const categorySelect = document.createElement('select');
  categorySelect.classList.add('form-input-select', 'padding10', 'width100');
  const option1 = document.createElement('option');
  option1.value = 'ZLECĘ';
  option1.textContent = 'ZLECĘ';
  const option2 = document.createElement('option');
  option2.value = 'WYKONAM';
  option2.textContent = 'WYKONAM';
  categorySelect.appendChild(option1);
  categorySelect.appendChild(option2);
  contentWrapper.appendChild(categorySelect);

  const contentHeading = document.createElement('h3');
  contentHeading.classList.add('white', 'width100', 'margin-top');
  contentHeading.textContent = 'Treść';
  contentWrapper.appendChild(contentHeading);

  const contentTextarea = document.createElement('textarea');
  contentTextarea.classList.add('form-input-textarea', 'width100');
  contentTextarea.type = 'text';
  contentTextarea.rows = '10';
  contentWrapper.appendChild(contentTextarea);

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';
  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const createButton = document.createElement('button');
  createButton.classList.add('default-button');
  createButton.innerHTML = 'Utwórz<span class="material-symbols-outlined">add</span>';

  createButton.addEventListener('click', async () => {
    createButton.setAttribute('disabled', '');
    createButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const category = categorySelect.value;
    const content = contentTextarea.value;

    const rawData = {
      category: category,
      content: content,
    };
    const dataToSend = JSON.stringify(rawData);

    const response = await fetch(`/offers`, {
      method: 'POST',
      body: dataToSend,
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      const data = await response.json();
      window.location.href = `/offers/${data.id}`;
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      createButton.removeAttribute('disabled');
      createButton.innerHTML = 'Utwórz<span class="material-symbols-outlined">add</span>';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(createButton);
  popup.appendChild(buttonWrapper);

  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */

function createDeleteOfferPopup(offerId, content) {
  const popupWrapper = document.createElement('div');
  popupWrapper.classList.add('popup-wrapper');

  const popup = document.createElement('div');
  popup.classList.add('popup');

  const headerWrapper = document.createElement('div');
  headerWrapper.classList.add('width100', 'popup-header');

  const heading = document.createElement('h2');
  heading.classList.add('white', 'width100');
  heading.textContent = 'Usuń ogłoszenie';
  headerWrapper.appendChild(heading);

  const contentWrapper = document.createElement('div');
  contentWrapper.classList.add('width100', 'padding10', 'left-column-wrapper');

  const textParagraph = document.createElement('h3');
  textParagraph.classList.add('light-gray', 'width100');
  textParagraph.textContent = 'Jesteś pewien, że chcesz usunąć te ogłoszenie?';

  const contentParagraph = document.createElement('p');
  contentParagraph.classList.add('light-gray', 'width100');
  contentParagraph.textContent = content;

  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('right-row-wrapper', 'width100', 'padding10', 'flex-wrap');

  const cancelButton = document.createElement('button');
  cancelButton.classList.add('default-empty-button');
  cancelButton.innerHTML = '<span class="material-symbols-outlined">close</span>Anuluj';

  cancelButton.addEventListener('click', () => {
    document.body.removeChild(popupWrapper);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('default-button');
  deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';

  deleteButton.addEventListener('click', async () => {
    deleteButton.setAttribute('disabled', '');
    deleteButton.innerHTML =
      'Proszę czekać <span class="material-symbols-outlined loading-icon">progress_activity</span>';

    const response = await fetch(`/offers/${offerId}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      document.body.removeChild(popupWrapper);
      window.location.href = `/offers`;
    } else {
      const data = await response.json();
      const errorMessages = Array.isArray(data.message) ? data.message.join('<br>') : data.message;
      createAlert('red', errorMessages);

      deleteButton.removeAttribute('disabled');
      deleteButton.innerHTML = '<span class="material-symbols-outlined">delete</span>Usuń';
    }
  });

  buttonWrapper.appendChild(cancelButton);
  buttonWrapper.appendChild(deleteButton);

  popup.appendChild(headerWrapper);
  contentWrapper.appendChild(textParagraph);
  contentWrapper.appendChild(contentParagraph);
  popup.appendChild(contentWrapper);
  popup.appendChild(buttonWrapper);
  popupWrapper.appendChild(popup);

  document.body.appendChild(popupWrapper);
}

/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
/****************************************************************************** */
