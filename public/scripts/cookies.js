function createCookiesBox() {
  if (localStorage.getItem('cookiesAccepted')) {
    return;
  }

  const icon = document.createElement('span');
  icon.textContent = 'cookie';
  icon.className = 'material-symbols-outlined';

  const cookiesBox = document.createElement('div');
  cookiesBox.className = 'cookies-box';

  const cookiesText = document.createElement('p');
  cookiesText.textContent =
    'Używamy plików cookies, aby dostosować działanie strony do Twoich potrzeb.';

  const okButton = document.createElement('button');
  okButton.className = 'cookies-box-button';
  okButton.textContent = 'OK';

  okButton.addEventListener('click', () => {
    localStorage.setItem('cookiesAccepted', 'true');
    document.body.removeChild(cookiesBox);
  });

  cookiesBox.appendChild(icon);
  cookiesBox.appendChild(cookiesText);
  cookiesBox.appendChild(okButton);

  document.body.appendChild(cookiesBox);
}

createCookiesBox();
